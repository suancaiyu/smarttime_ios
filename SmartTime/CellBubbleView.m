//
//  CellBubbleView.m
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CellBubbleView.h"
#import "UILabel+Util.h"
#import "UIColor+Util.h"
#import "STimeTableData.h"
#import "SaveDiaryInfo.h"

#define TEXT_MARGIN_TOP     4
#define PADING_LEFT     11
#define PADING_RIGHT    8

#define LOCATION_IMG_WIDTH      14
#define LOCATION_IMG_HEIGHT     14
#define LOCATION_MARGIN_TOP     4

#define TIME_MARGIN_TOP     4
#define EMOAUDIO_MARGIN_TOP     7

#define EMOAUDIO_WIDTH      20
#define EMOAUDIO_HEIGHT     20

#define IMAGEAREA_MARGIN_TOP    4

@interface CellBubbleView(){
    
    CGFloat _width;
    NSInteger _index;
    SaveDiaryInfo *_diaryInfo;
    
}
@end

@implementation CellBubbleView

-(instancetype)initWithWidth:(CGFloat)givenWidth Index:(NSInteger)index{
    
    self = [super init];
    if (self) {
        _width = givenWidth;
        _index = index;
        // 初始化本条日记数据
        STimeTableData *tableData = [STimeTableData sharedInstance];
        _diaryInfo = [tableData.listData objectAtIndex:index];
        [self initUI];
    }
    
    return self;
}

-(void)initUI{
    
    _bubbleheight = 0.f;
    
    // 文字
    NSString *text = _diaryInfo.text;
    CGSize textSize = CGSizeZero;
    if (IS_DBTEXT_NOTEMP(_diaryInfo.text)) {
        _lbtext = [[UILabel alloc]initWithFrame:CGRectZero];
        _lbtext.text = text;
        _lbtext.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
        _lbtext.numberOfLines = 0;// 设置多行显示
        textSize = [_lbtext textAreaSizeWithWidth:_width-PADING_LEFT-PADING_RIGHT];
        _lbtext.frame = CGRectMake(PADING_LEFT, TEXT_MARGIN_TOP, textSize.width, textSize.height);
        [self addSubview:_lbtext];
    }
    _bubbleheight += textSize.height + (IS_DBTEXT_NOTEMP(_diaryInfo.text)?TEXT_MARGIN_TOP:0);
    
    // 图片
    CGFloat imgareaheight = 0.f;
    if (IS_DBIMGNUM_NOTZERO(_diaryInfo.imgnum)) {
        ImageAreaView *imagearea = [[ImageAreaView alloc]initWithWidth:_width-PADING_LEFT-PADING_RIGHT Index:_index];
        imagearea.frame = CGRectMake(PADING_LEFT, _bubbleheight+IMAGEAREA_MARGIN_TOP, _width-PADING_LEFT-PADING_RIGHT, imagearea.height);
        imgareaheight = imagearea.height;
        [self addSubview:imagearea];
    }
    
    _bubbleheight += imgareaheight + (IS_DBIMGNUM_NOTZERO(_diaryInfo.imgnum)?IMAGEAREA_MARGIN_TOP:0);
    
    
    // 地点条
    NSString *location = _diaryInfo.location;
    CGSize locationSize = CGSizeZero;
    if (IS_DBTEXT_NOTEMP(location)) {
        // 地点图标
        UIImageView *ivlocation = [[UIImageView alloc]initWithFrame:CGRectMake(PADING_LEFT, _bubbleheight+LOCATION_MARGIN_TOP+2, LOCATION_IMG_WIDTH, LOCATION_IMG_HEIGHT)];
        ivlocation.image = [UIImage imageNamed:@"item_location.png"];
        ivlocation.contentMode=UIViewContentModeScaleAspectFill;
        [self addSubview:ivlocation];
        
        // 地点文字
        _lblocation = [[UILabel alloc]initWithFrame:CGRectZero];
        _lblocation.text = location;
        _lblocation.textColor = [UIColor colorFromRGB:0x888888];
        _lblocation.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
        _lblocation.numberOfLines = 0;// 设置多行显示
        locationSize = [_lblocation textAreaSizeWithWidth:_width-PADING_LEFT-PADING_RIGHT-LOCATION_IMG_WIDTH];
        _lblocation.frame = CGRectMake(PADING_LEFT+LOCATION_IMG_WIDTH, _bubbleheight+LOCATION_MARGIN_TOP, locationSize.width, locationSize.height);
        [self addSubview:_lblocation];
    }
    _bubbleheight += locationSize.height + (location.length>0?LOCATION_MARGIN_TOP:0);
    
    
    // 具体时间
    _lbtime = [[UILabel alloc]initWithFrame:CGRectZero];
    _lbtime.text = @"2015-05-21 15:14:48";
    _lbtime.textColor = [UIColor colorFromRGB:0x888888];
    _lbtime.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    CGSize timeSize = [_lbtime textAreaSize];
    _lbtime.frame = CGRectMake(PADING_LEFT+2, _bubbleheight+TIME_MARGIN_TOP+2, timeSize.width, timeSize.height);
    [self addSubview:_lbtime];
    // 表情
    _ivemo = [[UIImageView alloc]initWithFrame:CGRectMake(_width-PADING_RIGHT-2-EMOAUDIO_WIDTH, _bubbleheight+EMOAUDIO_MARGIN_TOP, EMOAUDIO_WIDTH, EMOAUDIO_HEIGHT)];
    _ivemo.image = [UIImage imageNamed:@"emotion_stub.png"];
    [self addSubview:_ivemo];
    // 录音
    _ivaudio = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_ivemo.frame)-16-EMOAUDIO_WIDTH, _bubbleheight+EMOAUDIO_MARGIN_TOP, EMOAUDIO_WIDTH, EMOAUDIO_HEIGHT)];
    _ivaudio.image = [UIImage imageNamed:@"voice_stub.png"];
    [self addSubview:_ivaudio];
    
    _bubbleheight += TIME_MARGIN_TOP + EMOAUDIO_HEIGHT;
    

    
    
    
    
}

@end
