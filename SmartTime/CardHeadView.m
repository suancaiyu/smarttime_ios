//
//  CardHeadView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CardHeadView.h"

#define CARDHEAD_HEIGHT      38

@interface CardHeadView(){
    
    NSInteger _curIndex;
}

@end

@implementation CardHeadView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _curIndex = 0;
        [self initUI];
    }
    return self;
}

-(void)initUI{
    
    CGFloat imgwidth = ViewWidth(self)/4;
    
    // 位置
    _ivlocation = [[UIImageView alloc]initWithFrame:Rect(4, 4, imgwidth-8, CARDHEAD_HEIGHT-8)];
    _ivlocation.image = [UIImage imageNamed:@"edit_location"];
    _ivlocation.contentMode = UIViewContentModeScaleAspectFit;
    _ivlocation.tag = 0;
    [self addSubview:_ivlocation];
    
    // 照片
    _ivphoto = [[UIImageView alloc]initWithFrame:Rect(imgwidth+4, 4, imgwidth-8, CARDHEAD_HEIGHT-8)];
    _ivphoto.image = [UIImage imageNamed:@"edit_photo"];
    _ivphoto.contentMode = UIViewContentModeScaleAspectFit;
    _ivphoto.tag = 1;
    [self addSubview:_ivphoto];
    
    // 天气
    _ivweather = [[UIImageView alloc]initWithFrame:Rect(imgwidth*2+4, 4, imgwidth-8, CARDHEAD_HEIGHT-8)];
    _ivweather.image = [UIImage imageNamed:@"edit_weather"];
    _ivweather.contentMode = UIViewContentModeScaleAspectFit;
    _ivweather.tag = 2;
    [self addSubview:_ivweather];
    
    // 表情
    _ivemotion = [[UIImageView alloc]initWithFrame:Rect(imgwidth*3+4, 4, imgwidth-8, CARDHEAD_HEIGHT-8)];
    _ivemotion.image = [UIImage imageNamed:@"edit_emotion"];
    _ivemotion.contentMode = UIViewContentModeScaleAspectFit;
    _ivemotion.tag = 3;
    [self addSubview:_ivemotion];
    
    
    // 分别添加单击手势
    UITapGestureRecognizer *locationrecog=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
    [_ivlocation setUserInteractionEnabled:YES];
    [_ivlocation addGestureRecognizer:locationrecog];
    
    UITapGestureRecognizer *photorecog=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
    [_ivphoto setUserInteractionEnabled:YES];
    [_ivphoto addGestureRecognizer:photorecog];
    
    UITapGestureRecognizer *wearecog=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
    [_ivweather setUserInteractionEnabled:YES];
    [_ivweather addGestureRecognizer:wearecog];
    
    UITapGestureRecognizer *emorecog=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
    [_ivemotion setUserInteractionEnabled:YES];
    [_ivemotion addGestureRecognizer:emorecog];
    
}

// 四个图标单机事件的处理
-(void)handlerTapGesture:(UIPanGestureRecognizer*)recognizer {
    
    if ([_controllerDelegate isKeyboardShown]) {
        // 隐藏键盘，调用代理的方法
        [_controllerDelegate hideKeyboard];
        return;
    }

    // 这里根据actualtag来判断是哪一个view
    NSInteger tag = recognizer.view.tag;
    NSInteger actualtag = recognizer.view.tag%10;
    
    if (actualtag == _curIndex) {
        
        switch (actualtag) {
            case 0:
                
                if (tag>=10) {
                    // 原来绿显
                    [_delegate clearCardView:actualtag];      // 清空卡片
                    [self setHeaderIndex:actualtag Green:NO]; // 灰显
                }else{
                    // 原来灰显。调用代理获取位置
                    [_controllerDelegate requireLocation];
                }
                break;
            case 1:
                
                // do nothing
                break;
            case 2:
            case 3:
                
                [_delegate clearCardView:actualtag];      // 清空卡片
                [self setHeaderIndex:actualtag Green:NO]; // 灰显
                break;
                
            default:
                break;
        }
    }else{
        
        // 非当前卡片，滚动到对应card
        [self.delegate scrollToCard:actualtag];
        _curIndex = actualtag;
    }
    
}

/**
 *  设置卡片栏按钮颜色显示
 *
 *  @param index   索引
 *  @param isGreen 是否绿显
 */
-(void)setHeaderIndex:(NSInteger)index Green:(BOOL)isGreen{
    
    switch (index) {
        case 0:
            _ivlocation.image = [UIImage imageNamed:isGreen?@"edit_location_green.png":@"edit_location.png"];
            _ivlocation.tag = isGreen?10:0;
            break;
        case 1:
            _ivphoto.image = [UIImage imageNamed:isGreen?@"edit_photo_green.png":@"edit_photo.png"];
            _ivphoto.tag = isGreen?11:1;
            break;
        case 2:
            _ivweather.image = [UIImage imageNamed:isGreen?@"edit_weather_green.png":@"edit_weather.png"];
            _ivweather.tag = isGreen?12:2;
            break;
        case 3:
            _ivemotion.image = [UIImage imageNamed:isGreen?@"edit_emotion_green.png":@"edit_emotion.png"];
            _ivemotion.tag = isGreen?13:3;
            break;
            
        default:
            break;
    }
}

@end
