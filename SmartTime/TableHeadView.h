//
//  TableHeadView.h
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "DiarySummaryView.h"

@interface TableHeadView : UIView

@property (nonatomic, strong)UIImageView *firstpage;
@property (nonatomic, strong)UIImageView *head;
@property (nonatomic, strong)DiarySummaryView *diarySummaryView;

-(instancetype)initWithFrame:(CGRect)frame;

@end
