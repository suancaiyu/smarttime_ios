//
//  UILabel+Util.m
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "UILabel+Util.h"

@implementation UILabel(Util)

/**
 *  根据UIlabel的文字内容、字体，得到占有的区域尺寸。
 *  使用NSString的sizeWithAttributes
 *
 *  @return 尺寸
 */
-(CGSize)textAreaSize{
    
    return [self.text sizeWithAttributes:[NSDictionary dictionaryWithObjectsAndKeys:self.font, NSFontAttributeName, nil]];
}

/**
 *  给定文字区域的显示宽度，文字超过一行（width）会换行，返回文字区域大小
 *
 *  @param width 给定宽度
 *
 *  @return 尺寸
 */
-(CGSize)textAreaSizeWithWidth:(CGFloat)width{
    
    return [self.text boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingTruncatesLastVisibleLine |
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObjectsAndKeys:self.font, NSFontAttributeName, nil] context:nil].size;
}

@end
