//
//  CellBubbleView.h
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "ImageAreaView.h"

@interface CellBubbleView : UIView

@property (nonatomic, assign)CGFloat bubbleheight;

@property (nonatomic, strong)UILabel *lbtext;
@property (nonatomic, strong)ImageAreaView *imagearea;
@property (nonatomic, strong)UILabel *lblocation;
//@property (nonatomic, strong)UIView *bgbubble;
@property (nonatomic, strong)UILabel *lbtime;
@property (nonatomic, strong)UIImageView *ivemo;
@property (nonatomic, strong)UIImageView *ivaudio;

-(instancetype)initWithWidth:(CGFloat)givenWidth Index:(NSInteger)index;

@end
