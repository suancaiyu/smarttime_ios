//
//  BottomView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "BottomView.h"
#import "UIColor+Util.h"

#define DEVIDER_HEIGHT     0.5

@implementation BottomView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    
    return self;
}

-(void)initUI{
    
    // 整个区域的分割线
    UIView *divider = [[UIView alloc]initWithFrame:Rect(0, 0, ViewWidth(self), DEVIDER_HEIGHT)];
    divider.backgroundColor = [UIColor colorFromRGB:0x888888];
    [self addSubview:divider];
    
    // 卡片栏
    _cardHead = [[CardHeadView alloc]initWithFrame:CGRectMake(0, DEVIDER_HEIGHT, ViewWidth(self), 38)];
    _cardHead.delegate = self;
    [self addSubview:_cardHead];
    
    // 分隔线
    UIImageView *carddevider = [[UIImageView alloc]initWithFrame:Rect(0, DEVIDER_HEIGHT+38, ViewWidth(self), 1)];
    carddevider.image = [UIImage imageNamed:@"list_divider.png"];
    [self addSubview:carddevider];
    
    // 卡片
    _cardsView = [[CardsScrollView alloc]initWithFrame:Rect(0, DEVIDER_HEIGHT+38+1, ViewWidth(self), 220)];
    _cardsView.headerDeledate = self;   // 重写代理的setter函数，设置四张卡片的代理
    [self addSubview:_cardsView];
    
}

#pragma mark - CardHeaderProtocol
-(void)scrollToCard:(NSInteger)index{
    
    [_cardsView setCurIndex:index];
}
-(void)clearCardView:(NSInteger)index{
    
    [_cardsView clearCardView:index];
}

#pragma mark - CardHearColorDelegate
-(void)setCardHeadColor:(NSInteger)index Green:(BOOL)isGreen{
    
    [_cardHead setHeaderIndex:index Green:isGreen];
    
}

-(void)updateLocationCard:(NSString*)location{
    
    [_cardsView updateLocationCard:location];// 更新位置卡片显示
    [self setCardHeadColor:0 Green:YES];// 按钮绿显
}

-(void)setControllerDelegate:(id<CardHeaderControllerProtocol>)controllerDelegate{
    _cardHead.controllerDelegate = controllerDelegate;
}

-(void)setDataDelegate:(id<CardsDataControllerDelegate>)dataDelegate{
    _cardsView.dataDelegate = dataDelegate;
}

@end
