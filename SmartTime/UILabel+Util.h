//
//  UILabel+Util.h
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface UILabel(Util)

-(CGSize)textAreaSize;
-(CGSize)textAreaSizeWithWidth:(CGFloat)width;

@end
