//
//  LocalImageUtil.m
//  SmartTime
//
//  Created by tsia on 15-6-11.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "LocalImageUtil.h"

@implementation LocalImageUtil

/**
 *  读取本地图片，并显示到ImageView上
 *
 *  @param path 本地路径，如 assets-library://asset/asset.JPG?id=224D9935-3252-4E8E-95D8-FEFB834A5DEE&ext=JPG
 *  @param iv   要加入图片的ImageView
 */
+(void)readImageFromLocalPath:(NSURL*)path ToImageView:(UIImageView*)iv{
    
    ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
    
    [assetLibrary assetForURL:path resultBlock:^(ALAsset *asset) {
        
        ALAssetRepresentation *assetRep = [asset defaultRepresentation];    // 原图
        CGImageRef imgRef = [assetRep fullResolutionImage];
        UIImage *image = [UIImage imageWithCGImage:imgRef scale:assetRep.scale orientation:(UIImageOrientation)assetRep.orientation];
        
        iv.image = image;
    } failureBlock:^(NSError *error) {
        
        NSLog(@"error=%@",error);
    }];
}

/**
 *  支持的照片源类型
 */
+(void)checkSupportSource{
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        NSLog(@"支持相机");
    }
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        NSLog(@"支持图库");
    }
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
        NSLog(@"支持相片库");
    }
}

+(BOOL)isSupportSourceCamera{
    BOOL b = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (b) {
        NSLog(@"支持相机");
    }
    return b;
}
+(BOOL)isSupportSourcePhotoLibrary{
    BOOL b = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
    if (b) {
        NSLog(@"支持图库");
    }
    return b;
}
+(BOOL)isSupportSourcePhotosAlbum{
    BOOL b = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    if (b) {
        NSLog(@"支持相片库");
    }
    return b;
}


@end
