//
//  ReadEditViewController.h
//  SmartTime
//
//  Created by tsia on 15-6-6.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CustomViewController.h"
#import "BottomView.h"
#import "LocationService.h"

@interface ReadEditViewController : CustomViewController <CardHeaderControllerProtocol, CardsDataControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, LocationServiceDoUI>

// 主页打开时设置datetimekey，nil为创建模式，否则就是阅读模式对应的日记datetime作为key
@property (nonatomic, strong) NSString *datetimekey;
@property (nonatomic, assign) NSInteger rowidx;    // 仅阅读模式第一次打开有用

@end
