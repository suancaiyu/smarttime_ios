//
//  CustomNaviController.h
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface CustomNaviController : UINavigationController

- (void)navigationCanDragBack:(BOOL)bCanDragBack;

@end
