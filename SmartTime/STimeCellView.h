//
//  STimeCellView.h
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CellLeftView.h"
#import "TimeLineView.h"
#import "CellBubbleView.h"

@interface STimeCellView : UIView

@property (nonatomic, assign)CGFloat cellheight;
@property (nonatomic, strong)CellLeftView *leftView;
@property (nonatomic, strong)TimeLineView *timeLineView;
@property (nonatomic, strong)CellBubbleView *bubbleView;

-(id)initWithIndex:(NSInteger)index;

@end
