//
//  CellLeftView.h
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@interface CellLeftView : UIView

@property (nonatomic, assign) CGSize framesize;

-(instancetype)initWithIndex:(NSInteger)index;

@end
