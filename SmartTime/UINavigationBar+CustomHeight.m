//
//  UINavigationBar+CustomHeight.m
//  SmartTime
//
//  Created by tsia on 15-6-4.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "UINavigationBar+CustomHeight.h"

@implementation UINavigationBar(CustomHeight)

/**
 *  重写layoutSubviews方法
 */
-(void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect barframe = self.frame;
    barframe.size.height = StatusBarHeight + STimeNaviBarHeight;// 自定义的导航栏高度
    self.frame = barframe;
    
    //DebugLog(@"layoutSubviews");
    
}

@end
