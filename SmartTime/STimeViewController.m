//
//  STimeViewController.m
//  SmartTime
//
//  Created by tsia on 15/5/29.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "STimeViewController.h"
#import "STimeCellView.h"
#import "TableHeadView.h"
#import "UIColor+Util.h"
#import "STimeTableData.h"
#import "UINavigationBar+CustomHeight.h"
#import "ReadEditViewController.h"
#import "SaveDiaryInfo.h"

@interface STimeViewController (){
    
    STimeCellView *_cellview;   // 临时的对象，通过初始化它得到每一行的高度
}

@end

@implementation STimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // view背景白色，view包含window的整个区域
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    
    // 时光轴列表（对于一般的如UIImageView，都是从屏幕左上角开始。这里tableView是从状态栏下面开始）
    _stimetable = [[UITableView alloc]initWithFrame:CGRectMake(0.f, 0.f, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    _stimetable.delegate = self;
    _stimetable.dataSource = self;
    _stimetable.tableHeaderView = [[TableHeadView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 245)];
    _stimetable.separatorStyle = UITableViewCellSeparatorStyleNone;//隐藏默认的分割线
    [self.view addSubview:_stimetable];
    
    _stimetable.contentInset = UIEdgeInsetsMake(48, 0, 0, 0);
    _stimetable.contentOffset = CGPointMake(0, -48);
    
    
    
    [self setNaviBarLeftFirstIv:[CustomNaviBarView createNaviBarImageView:@"stime_setting" target:self action:@selector(tapSetting:)]];
    [self setNaviBarLeftSecondIv:[CustomNaviBarView createNaviBarImageView:@"stime_smartlogo" target:self action:nil]];
    
    [self setNaviBarRightFirstIv:[CustomNaviBarView createNaviBarImageView:@"stime_create" target:self action:@selector(tapCreate:)]];
    [self setNaviBarRightSecondIv:[CustomNaviBarView createNaviBarImageView:@"stime_search" target:self action:@selector(tapSearch:)]];
    
    NSLog(@"viewDidLoad");
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
    
    // 刷新日记数据列表
    STimeTableData *tabledata = [STimeTableData sharedInstance];
    [tabledata freshData];
    
    // 然后刷新tableview
    [_stimetable reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear");
}


// 设置的点击事件
-(void)tapSetting:(UIPanGestureRecognizer*)recognizer{
    
    NSLog(@"setting click%@", recognizer);
}

// 新建点击事件
-(void)tapCreate:(UIPanGestureRecognizer*)recognizer{
    
    ReadEditViewController *readEdit = [[ReadEditViewController alloc]init];
    readEdit.datetimekey = nil;       // nil表示创建模式
    [self.navigationController pushViewController:readEdit animated:YES];
}

// 搜索点击事件
-(void)tapSearch:(UIPanGestureRecognizer*)recognizer{
    
    NSLog(@"setting click%@", recognizer);
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [STimeTableData sharedInstance].listData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // 声明静态字符串型对象，用来标记重用单元格
    static NSString *STimeTableIdentifier = @"STimeTableIdentifier";
    // 用STimeTableIdentifier表示需要重用的单元
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:STimeTableIdentifier];
    
    // 如果如果没有多余单元，则需要创建新的单元
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:STimeTableIdentifier];
    }else{
        
        // 先清除所有的子控件，才能添加新的内容.
        [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    // 取消选中时的颜色
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    // 通过添加子view自定义contentView
    NSInteger row = [indexPath row];
    STimeCellView *cellview = [[STimeCellView alloc]initWithIndex:row];
    [cell.contentView addSubview:cellview];
    //[cellview setFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"estimatedHeightForRowAtIndexPath-------------%ld", (long)[indexPath row]);
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // 通过初始化临时对象得到每一行的高度
    NSInteger row = [indexPath row];
    _cellview = [[STimeCellView alloc]initWithIndex:row];
    
    NSLog(@"%li", (long)row);
    
    return _cellview.cellheight;
}

// 选中单元格事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = [indexPath row];
    
    // 得到这条日记的datetime
    SaveDiaryInfo *diaryinfo = [STimeTableData sharedInstance].listData[row];
    // 阅读模式打开，传入datetime
    ReadEditViewController *readEdit = [[ReadEditViewController alloc]init];
    readEdit.datetimekey = diaryinfo.datetime;      // nil表示阅读模式
    readEdit.rowidx = row;
    [self.navigationController pushViewController:readEdit animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
