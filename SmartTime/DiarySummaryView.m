//
//  DiarySummaryView.m
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "DiarySummaryView.h"
#import "UIColor+Util.h"
#import "UILabel+Util.h"
#import "STimeTableData.h"

@interface DiarySummaryView(){
    
    UILabel *text1;  //私有的成员变量
    UILabel *text2;
    UILabel *text3;
    CGFloat largeSize;
    CGFloat smallSize;
    
    NSString *_sumDay;
    NSString *_sumDiary;
    NSString *_sumImage;
    
}

@end

@implementation DiarySummaryView

-(instancetype)initWithWidth:(CGFloat)givenWidth{
    
    self = [super init];
    if (self) {
        [self initData];
        [self initUI];
        [self fitTextWitWidth:givenWidth];
    }
    
    return self;
}

-(void)initData{
    
    //初始化默认的字体大小
    largeSize = 20.f;
    smallSize = 14.f;
    
    STimeTableData *tableData = [STimeTableData sharedInstance];
    _sumDay = tableData.sumDay;
    _sumDiary = tableData.sumDiary;
    _sumImage = tableData.sumImage;
    
}

-(void)initUI{
    
    // 总天数
    _daysum = [[UILabel alloc]initWithFrame:CGRectZero];
    _daysum.text = _sumDay;
    _daysum.textColor = [UIColor colorFromRGB:0x008b00];
    _daysum.font = [UIFont fontWithName:@"HelveticaNeue" size:largeSize];
    CGSize daysumSize = [_daysum textAreaSize];
    _daysum.frame = Rect(0, 0, daysumSize.width, daysumSize.height);
    [self addSubview:_daysum];
    
    // 天时光里，共
    text1 = [[UILabel alloc]initWithFrame:CGRectZero];
    text1.text = @"天时光里，共";
    text1.font = [UIFont fontWithName:@"HelveticaNeue" size:smallSize];
    CGSize text1Size = [text1 textAreaSize];
    text1.frame = Rect(daysumSize.width, CGRectGetMaxY(_daysum.frame)-2-text1Size.height, text1Size.width, text1Size.height);
    [self addSubview:text1];
    
    // 日记总数
    _diarysum = [[UILabel alloc]initWithFrame:CGRectZero];
    _diarysum.text = _sumDiary;
    _diarysum.textColor = [UIColor colorFromRGB:0x008b00];
    _diarysum.font = [UIFont fontWithName:@"HelveticaNeue" size:largeSize];
    CGSize diarysumSize = [_diarysum textAreaSize];
    _diarysum.frame = Rect(CGRectGetMaxX(text1.frame), 0, diarysumSize.width, diarysumSize.height);
    [self addSubview:_diarysum];
    
    // 篇日记，
    text2 = [[UILabel alloc]initWithFrame:CGRectZero];
    text2.text = @"篇日记，";
    text2.font = [UIFont fontWithName:@"HelveticaNeue" size:smallSize];
    CGSize text2Size = [text2 textAreaSize];
    text2.frame = Rect(CGRectGetMaxX(_diarysum.frame), CGRectGetMaxY(_daysum.frame)-2-text2Size.height, text2Size.width, text2Size.height);
    [self addSubview:text2];
    
    // 图片数
    _imagesum = [[UILabel alloc]initWithFrame:CGRectZero];
    _imagesum.text = _sumImage;
    _imagesum.textColor = [UIColor colorFromRGB:0x008b00];
    _imagesum.font = [UIFont fontWithName:@"HelveticaNeue" size:largeSize];
    CGSize imagesumSize = [_imagesum textAreaSize];
    _imagesum.frame = Rect(CGRectGetMaxX(text2.frame), 0, imagesumSize.width, imagesumSize.height);
    [self addSubview:_imagesum];
    
    // 张照片
    text3 = [[UILabel alloc]initWithFrame:CGRectZero];
    text3.text = @"张照片";
    text3.font = [UIFont fontWithName:@"HelveticaNeue" size:smallSize];
    CGSize text3Size = [text3 textAreaSize];
    text3.frame = Rect(CGRectGetMaxX(_imagesum.frame), CGRectGetMaxY(_daysum.frame)-2-text3Size.height, text3Size.width, text3Size.height);
    [self addSubview:text3];
    
    // 计算frameSize，调整后的宽度有可能比给定宽度小一点
    CGFloat sumwidth = _daysum.frame.size.width+text1.frame.size.width+_diarysum.frame.size.width+text2.frame.size.width+_imagesum.frame.size.width+text3.frame.size.width;
    _framesize = CGSizeMake(sumwidth, _daysum.frame.size.height);
    
}

/**
 *  根据给定宽度自适应文字的大小
 *
 *  @param givenWidth 父视图给定宽度
 */
-(void)fitTextWitWidth:(CGFloat)givenWidth{
    
    
    while (_framesize.width>givenWidth) {
        largeSize--;
        smallSize--;
        
        // 先移除所有原来的子控件避免重复创建，然后重新初始化UI
        [[self subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self initUI];

    }
    
}

@end
