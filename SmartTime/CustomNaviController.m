//
//  CustomNaviController.m
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CustomNaviController.h"

@interface CustomNaviController ()

@end

@implementation CustomNaviController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavigationBarHidden:NO];       // 使导航条有效
    [self.navigationBar setHidden:YES];     // 隐藏导航条，但由于导航条有效，系统的返回按钮页有效，所以可以使用系统的右滑返回手势。
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// 是否可右滑返回
- (void)navigationCanDragBack:(BOOL)bCanDragBack{
    
    if (IsiOS7Later){
        self.interactivePopGestureRecognizer.enabled = bCanDragBack;
    }
}

@end
