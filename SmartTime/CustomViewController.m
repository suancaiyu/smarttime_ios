//
//  CustomViewController.m
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CustomViewController.h"

@interface CustomViewController ()

@property (nonatomic, readonly) CustomNaviBarView *naviBar;   // 私有

@end

@implementation CustomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // 在ViewController下添加子view默认从屏幕左上角开始
    _naviBar = [[CustomNaviBarView alloc]initWithFrame:Rect(0, 0, ScreenWidth, StatusBarHeight + STimeNaviBarHeight)];
    _naviBar.parentViewController = self;
    
    [self.view addSubview:_naviBar];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // 导航栏置于顶层
    if(_naviBar && !_naviBar.hidden){
        [self.view bringSubviewToFront:_naviBar];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 设置显示的标题文字
-(void)setNaviBarTitle:(NSString*)title{
    
    if (_naviBar){
        [_naviBar setTitle:title];
    }
}

-(void)setNaviBarLeftFirstIv:(UIImageView *)iv{
    
    if (_naviBar){
        [_naviBar setLeftFirstImageView:iv];
    }
}

-(void)setNaviBarLeftSecondIv:(UIImageView *)iv{
    if (_naviBar){
        [_naviBar setLeftSecondImageView:iv];
    }
}

-(void)setNaviBarRightFirstIv:(UIImageView *)iv{
    if (_naviBar){
        [_naviBar setRightFirstImage:iv];
    }
}

-(void)setNaviBarRightSecondIv:(UIImageView *)iv{
    if (_naviBar){
        [_naviBar setRightSecondImage:iv];
    }
}



@end
