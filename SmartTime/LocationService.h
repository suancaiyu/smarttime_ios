//
//  LocationService.h
//  SmartTime
//
//  Created by tsia on 15-6-15.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@protocol LocationServiceDoUI <NSObject>

-(void)updateLocationForUI:(NSString*)location IsSuccess:(BOOL)issucceed;

@end

@interface LocationService : NSObject

@property (nonatomic, assign) id<LocationServiceDoUI> delegate;

+(LocationService *)sharedInstance;
-(void)startLocate;

@end
