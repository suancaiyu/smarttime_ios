//
//  DiarySummaryView.h
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@interface DiarySummaryView : UIView

@property (nonatomic, strong)UILabel *daysum;
@property (nonatomic, strong)UILabel *diarysum;
@property (nonatomic, strong)UILabel *imagesum;

@property (nonatomic, assign)CGSize framesize;

-(instancetype)initWithWidth:(CGFloat)givenWidth;

@end
