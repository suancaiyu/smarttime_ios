//
//  ImageAreaView.m
//  SmartTime
//
//  Created by tsia on 15-6-2.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "ImageAreaView.h"
#import "STimeTableData.h"
#import "SaveDiaryInfo.h"

@interface ImageAreaView(){
    
    CGFloat _width;
    NSInteger _index;
    SaveDiaryInfo *_diaryInfo;
    
    // 各图片的尺寸 pt
    CGFloat oneimg_width;
    CGFloat oneimg_height;
    
    CGFloat twoimg_size;
    
    CGFloat threeimg_largeSize;
    CGFloat threeimg_smallSize;
    
    CGFloat fourimg_smallSize;
}

@end

@implementation ImageAreaView

-(instancetype)initWithWidth:(CGFloat)givenwidth Index:(NSInteger)index{
    
    self = [super init];
    if (self) {
        _width = givenwidth;
        _index = index;
        // 初始化本条日记数据
        STimeTableData *tableData = [STimeTableData sharedInstance];
        _diaryInfo = [tableData.listData objectAtIndex:index];
        [self initUI];
    }
    
    return self;
}

-(void)initUI{
    
    NSInteger imgnum = [_diaryInfo.imgnum integerValue];// 如果字符串不为数字，返回0
    
    [self calculateImageSize:_width];
    
    switch (imgnum) {
        case 0:
            // 0张图片
            _height = 0.f;
            
            break;
        case 1:
        {
            // 1张图片
            UIImageView *one_first = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, _width, 138)];
            one_first.image = [UIImage imageNamed:@"firstpage.jpg"];
            one_first.contentMode = UIViewContentModeScaleAspectFill;
            one_first.clipsToBounds = true;
            [self addSubview:one_first];
            
            _height = oneimg_height;
        }
            break;
        case 2:
        {
            // 2张图片
            UIImageView *two_first = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, twoimg_size, twoimg_size)];
            two_first.image = [UIImage imageNamed:@"firstpage.jpg"];
            two_first.contentMode = UIViewContentModeScaleAspectFill;
            two_first.clipsToBounds = true;
            [self addSubview:two_first];
            
            UIImageView *two_second = [[UIImageView alloc]initWithFrame:CGRectMake(twoimg_size+2, 0, twoimg_size, twoimg_size)];
            two_second.image = [UIImage imageNamed:@"firstpage.jpg"];
            two_second.contentMode = UIViewContentModeScaleAspectFill;
            two_second.clipsToBounds = true;
            [self addSubview:two_second];
            
            _height = twoimg_size;
        }
            break;
        case 3:
        {
            // 3张图片
            UIImageView *three_first = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, threeimg_largeSize, threeimg_largeSize)];
            three_first.image = [UIImage imageNamed:@"firstpage.jpg"];
            three_first.contentMode = UIViewContentModeScaleAspectFill;
            three_first.clipsToBounds = true;
            [self addSubview:three_first];
            
            UIImageView *three_second = [[UIImageView alloc]initWithFrame:CGRectMake(threeimg_largeSize+2, 0, threeimg_smallSize, threeimg_smallSize)];
            three_second.image = [UIImage imageNamed:@"firstpage.jpg"];
            three_second.contentMode = UIViewContentModeScaleAspectFill;
            three_second.clipsToBounds = true;
            [self addSubview:three_second];
            
            UIImageView *three_third = [[UIImageView alloc]initWithFrame:CGRectMake(threeimg_largeSize+2, threeimg_smallSize+2, threeimg_smallSize, threeimg_smallSize)];
            three_third.image = [UIImage imageNamed:@"firstpage.jpg"];
            three_third.contentMode = UIViewContentModeScaleAspectFill;
            three_third.clipsToBounds = true;
            [self addSubview:three_third];
            
            _height = threeimg_largeSize;
        }
            break;
        case 4:
        {
            // 4张图片
            UIImageView *four_first = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, fourimg_smallSize, fourimg_smallSize)];
            four_first.image = [UIImage imageNamed:@"firstpage.jpg"];
            four_first.contentMode = UIViewContentModeScaleAspectFill;
            four_first.clipsToBounds = true;
            [self addSubview:four_first];
            
            UIImageView *four_second = [[UIImageView alloc]initWithFrame:CGRectMake(fourimg_smallSize+2, 0, fourimg_smallSize, fourimg_smallSize)];
            four_second.image = [UIImage imageNamed:@"firstpage.jpg"];
            four_second.contentMode = UIViewContentModeScaleAspectFill;
            four_second.clipsToBounds = true;
            [self addSubview:four_second];
            
            UIImageView *four_third = [[UIImageView alloc]initWithFrame:CGRectMake(0, fourimg_smallSize+2, fourimg_smallSize, fourimg_smallSize)];
            four_third.image = [UIImage imageNamed:@"firstpage.jpg"];
            four_third.contentMode = UIViewContentModeScaleAspectFill;
            four_third.clipsToBounds = true;
            [self addSubview:four_third];
            
            UIImageView *four_forth = [[UIImageView alloc]initWithFrame:CGRectMake(fourimg_smallSize+2, fourimg_smallSize+2, fourimg_smallSize, fourimg_smallSize)];
            four_forth.image = [UIImage imageNamed:@"firstpage.jpg"];
            four_forth.contentMode = UIViewContentModeScaleAspectFill;
            four_forth.clipsToBounds = true;
            [self addSubview:four_forth];
            
            _height = _width;
            
        }
            break;
            
        default:
            break;
    }
    
}

-(void)calculateImageSize:(CGFloat)givenwidth{
    
    //一张图片
    oneimg_width = givenwidth;
    oneimg_height = 138;
    
    //两张图片
    twoimg_size = (givenwidth-2)/2;
    
    //三张图片
    threeimg_smallSize = (givenwidth-4)/3;
    threeimg_largeSize = 2*threeimg_smallSize+2;
    
    //四张图片
    fourimg_smallSize = (givenwidth-2)/2;
}

@end
