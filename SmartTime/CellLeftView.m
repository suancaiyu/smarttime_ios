//
//  CellLeftView.m
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CellLeftView.h"
#import "UIColor+Util.h"
#import "UILabel+Util.h"
#import "SaveDiaryInfo.h"
#import "STimeTableData.h"
#import "DateTimeUtil.h"

@interface CellLeftView(){
    
    SaveDiaryInfo *_diaryInfo;
}

@end

@implementation CellLeftView

-(instancetype)initWithIndex:(NSInteger)index{
    
    self = [super init];
    if (self) {
        
        // 初始化本条日记数据
        STimeTableData *tableData = [STimeTableData sharedInstance];
        _diaryInfo = [tableData.listData objectAtIndex:index];
        
        [self initUI];
    }
    
    return self;
}

/**
 *  左边时间栏view，适应方式：wrap内容
 */
-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    // 日
    UILabel *dayLabel = [[UILabel alloc]init];
    dayLabel.text = [DateTimeUtil dayFromDateTime:_diaryInfo.datetime];
    dayLabel.textColor = [UIColor colorFromRGB:0x008b00];
    dayLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:23.0f];
    CGSize daysize = [dayLabel textAreaSize];
    [dayLabel setFrame:CGRectMake(5, 10, daysize.width, daysize.height)];
    [self addSubview:dayLabel];
    
    // 月
    UILabel *monthLabel = [[UILabel alloc]init];
    monthLabel.text = [NSString stringWithFormat:@"/%@", [DateTimeUtil monthFromDateTime:_diaryInfo.datetime]];
    monthLabel.textColor = [UIColor colorFromRGB:0x777777];
    monthLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    CGSize monthsize = [monthLabel textAreaSize];
    [monthLabel setFrame:Rect(CGRectGetMaxX(dayLabel.frame), CGRectGetMaxY(dayLabel.frame)-4-monthsize.height, monthsize.width, monthsize.height)];
    [self addSubview:monthLabel];
    
    // 周
    UILabel *weekLabel = [[UILabel alloc]init];
    weekLabel.text = [DateTimeUtil weekFromDateTime:_diaryInfo.datetime];;
    weekLabel.textColor = [UIColor colorFromRGB:0x999999];
    weekLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    CGSize weeksize = [weekLabel textAreaSize];
    [weekLabel setFrame:Rect(16, 10+dayLabel.frame.size.height, weeksize.width, weeksize.height)];
    [self addSubview:weekLabel];
    
    // 布局完毕，设置整个区域的大小
    self.framesize = Size(5+dayLabel.frame.size.width+monthLabel.frame.size.width, 10+dayLabel.frame.size.height+weekLabel.frame.size.height);

}

@end
