//
//  LocationCardView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardsViewDelegate.h"

@interface LocationCardView : UIView

@property (nonatomic, assign) id<CardHearColorDelegate> headerDelegte;
@property (nonatomic, assign) id<CardsDataControllerDelegate> dataDelegate;

-(void)clearLocation;
-(void)updateLocation:(NSString*)location;

@end
