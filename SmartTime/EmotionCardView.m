//
//  EmotionCardView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "EmotionCardView.h"
#import "LBorderView.h"

#define ROWS            3
#define COLUMNS         4

#define IMAGE_SIZE      24      // ImageView的宽高，图片边长不会超过48px，使用UIViewContentModeScaleAspectFit
#define MARGINING       4


@interface EmotionCardView(){
    
    CGFloat _viewwidth;
    CGFloat _viewheight;
}

@property (nonatomic, strong) NSArray *arrImages;
@property (nonatomic, strong) LBorderView *borderView;      // 用来显示边框的view
@property (nonatomic, strong) NSArray *arrIvs;      // 显示Image的UIImageView

@end

@implementation EmotionCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initUI];
    }
    return self;
}

-(void)initData{
    
    // 边框view的宽高
    _viewwidth = (ViewWidth(self)-MARGINING*2)/COLUMNS;
    _viewheight = (ViewHeight(self)-MARGINING*2)/ROWS;
    
    _arrImages = @[@"emo_1", @"emo_2",@"emo_3",@"emo_4",@"emo_5",@"emo_6",@"emo_7",@"emo_8",@"emo_9",@"emo_10",@"emo_11",@"emo_12"];
    
    int count = ROWS*COLUMNS;
    NSMutableArray *arriv = [[NSMutableArray alloc]initWithCapacity:count];
    for (int i=0; i<count; i++) {
        
        UIImageView *iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:_arrImages[i]]];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.tag = i; // 0-11
        
        // 添加单击手势识别
        UITapGestureRecognizer *recognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
        iv.userInteractionEnabled = YES;
        [iv addGestureRecognizer:recognizer];
        
        [arriv addObject:iv];
    }
    
    _arrIvs = [[NSArray alloc]initWithArray:arriv];
}

-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];

    // 边框view
    _borderView = [[LBorderView alloc]initWithFrame:CGRectZero];
    _borderView.borderType = BorderTypeDashed;
    _borderView.dashPattern = 6;
    _borderView.spacePattern = 2;
    _borderView.borderWidth = 0.5;
    _borderView.cornerRadius = 1;
    [self addSubview:_borderView];
    
    // 布局表情图片
    for (int i=0; i<ROWS; i++) {
        for (int j=0; j<COLUMNS; j++) {
            NSInteger idx = COLUMNS*i + j;
            
            UIImageView *iv = _arrIvs[idx];
            iv.frame = Rect(MARGINING+_viewwidth*j +(_viewwidth-IMAGE_SIZE)/2, MARGINING+_viewheight*i+ (_viewheight-IMAGE_SIZE)/2, IMAGE_SIZE, IMAGE_SIZE);
            [self addSubview:iv];
        }
    }
    
}

-(void)setBordeViewFrameWithIndex:(NSInteger)index{
    
    NSInteger i = index/COLUMNS;
    NSInteger j = index%COLUMNS;
    
    _borderView.frame = Rect(MARGINING+_viewwidth*j, MARGINING+_viewheight*i, _viewwidth, _viewheight);
}

// 单击事件响应，根据tag判断哪一个view
-(void)handlerTapGesture:(UITapGestureRecognizer*)recognizer{
    
    NSInteger tag = recognizer.view.tag;
    // 添加边框
    [self setBordeViewFrameWithIndex:tag];
    
    // 表情图标绿显
    [_headerDelegte setCardHeadColor:3 Green:YES];

    // 保存表情文字
    [_dataDelegate setEmo:[[NSString alloc]initWithFormat:@"%ld", (long)tag]];
    
}

-(void)clearSelected{
    
    _borderView.frame = CGRectZero;
    // 清空保存的数据
    [_dataDelegate setEmo:nil];
}

@end
