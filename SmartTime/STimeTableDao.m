//
//  STimeTableDao.m
//  SmartTime
//
//  Created by tsia on 15-6-3.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "STimeTableDao.h"
#import <sqlite3.h>

#define DBNAME    @"STimeDb.sqlite"
#define TABLENAME @"listitem_table"

@interface STimeTableDao(){
    
    NSString *_dbPath;
    sqlite3 *_db;
}

@end

@implementation STimeTableDao


-(void)initDbPath{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [paths objectAtIndex:0];
    
    _dbPath =[documentsDir stringByAppendingPathComponent:DBNAME];
}

/**
 *  创建或打开沙盒路径下的STimeDb.sqlite数据库。如果不存在就创建，存在就打开。会得到sqlite3的句柄
 *
 *  @return 返回是否打开或者创建成功
 */
-(BOOL)openDb{
    
    // 初始化_dbPath
    if (_dbPath.length<=0) {
        [self initDbPath];
    }
    
    if (sqlite3_open([_dbPath UTF8String], &_db) != SQLITE_OK) {
        sqlite3_close(_db);
        DebugLog(@"数据库打开失败");
        
        return NO;
    }
    
    DebugLog(@"数据库创建或打开成功");
    return YES;
}

/**
 *  为STimeDb.sqlite数据库创建listitem_table数据表（如果不存在）
 *
 *  @return 返回数据表是否成功创建
 */
-(BOOL)createSTimeTable{
    
    char *err;
    /**
     *
     * id INTEGER (primary key autoincrement)
     * datetime     TEXT
     * week     TEXT
     * text     TEXT
     * location TEXT
     * weather  TEXT
     * emotion TEXT  表情
     * audio   TEXT  录音
     * imgnum  TEXT  照片数量
     * image1  TEXT  照片1的路径
     * image2  TEXT  照片2的路径
     * image3  TEXT  照片3的路径
     * image4  TEXT  照片4的路径
     *
     */
    const char* createSQL="create table if not exists listitem_table (id INTEGER primary key autoincrement, datetime TEXT, week TEXT,text TEXT, location TEXT, weather TEXT, emotion INTEGER, audio TEXT, imgnum INTEGER, image1 TEXT, image2 TEXT, image3 TEXT, image4 TEXT)";
    
    if (sqlite3_exec(_db, createSQL, NULL, NULL, &err)!=SQLITE_OK) {
        sqlite3_close(_db);
        DebugLog(@"数据表创建失败！：%@",[NSString stringWithUTF8String:err]);
        
        return NO;
    }
    
    DebugLog(@"数据表创建成功！");
    return YES;
}

/**
 *  创建或打开STimeDb.sqlite数据库，并创建listitem_table数据表（如果不存在）
 *
 *  @return 是否成功
 */
-(BOOL)openDbAndCreateSTimeTable{
    
    if ([self openDb]) {
        if([self createSTimeTable]){
            return YES;
        }
        return NO;
    }
    return NO;
}

/**
 *  查询listitem_table的所有数据，以SaveDiaryInfo数组返回
 *
 *  @return 成功返回数组，失败返回nil
 */
-(NSMutableArray*)queryAllItems{
    
    if (![self openDbAndCreateSTimeTable]) {
        return nil;
    }
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    NSString *sql = @"SELECT * FROM listitem_table ORDER BY datetime DESC";
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(_db, [sql UTF8String], -1, &statement, nil)==SQLITE_OK) {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            /**
             *
             * id INTEGER (primary key autoincrement)
             * datetime     TEXT
             * week     TEXT
             * text     TEXT
             * location TEXT
             * weather  TEXT
             * emotion TEXT  表情
             * audio   TEXT  录音
             * imgnum  TEXT  照片数量
             * image1  TEXT  照片1的路径
             * image2  TEXT  照片2的路径
             * image3  TEXT  照片3的路径
             * image4  TEXT  照片4的路径
             *
             */
            
            char *datetime = (char *)sqlite3_column_text(statement, 1);         // 从0开始
            char *week = (char *)sqlite3_column_text(statement, 2);
            char *text = (char *)sqlite3_column_text(statement, 3);
            char *location = (char *)sqlite3_column_text(statement, 4);
            char *weather = (char *)sqlite3_column_text(statement, 5);
            char *emotion = (char *)sqlite3_column_text(statement, 6);
            char *audio = (char *)sqlite3_column_text(statement, 7);
            char *imgnum = (char *)sqlite3_column_text(statement, 8);
            char *image1 = (char *)sqlite3_column_text(statement, 9);
            char *image2 = (char *)sqlite3_column_text(statement, 10);
            char *image3 = (char *)sqlite3_column_text(statement, 11);
            char *image4 = (char *)sqlite3_column_text(statement, 12);
            
            SaveDiaryInfo *diaryInfo = [[SaveDiaryInfo alloc]init];
            diaryInfo.datetime = [NSString stringWithUTF8String:datetime];
            diaryInfo.week = [NSString stringWithUTF8String:week];
            diaryInfo.text = [NSString stringWithUTF8String:text];
            diaryInfo.location = [NSString stringWithUTF8String:location];
            diaryInfo.weather = [NSString stringWithUTF8String:weather];
            diaryInfo.emotion = [NSString stringWithUTF8String:emotion];
            diaryInfo.audio = [NSString stringWithUTF8String:audio];
            diaryInfo.imgnum = [NSString stringWithUTF8String:imgnum];
            diaryInfo.image1 = [NSString stringWithUTF8String:image1];
            diaryInfo.image2 = [NSString stringWithUTF8String:image2];
            diaryInfo.image3 = [NSString stringWithUTF8String:image3];
            diaryInfo.image4 = [NSString stringWithUTF8String:image4];
            
            [arr addObject:diaryInfo];
            
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(_db);
    
    return arr;
}

/**
 *  插入一条日记数据
 *
 *  @param diaryInfo SaveDiaryInfo对象
 *
 *  @return 是否成功插入
 */
-(BOOL)insertItem:(SaveDiaryInfo*)diaryInfo{
    
    if (![self openDbAndCreateSTimeTable]) {
        return NO;
    }
    
    char *err;
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO '%@' ('%@', '%@', '%@','%@', '%@','%@', '%@','%@', '%@','%@', '%@','%@') VALUES('%@', '%@', '%@','%@', '%@','%@', '%@','%@', '%@','%@', '%@','%@')", TABLENAME,
                     @"datetime",@"week",@"text",@"location",@"weather",@"emotion",@"audio",@"imgnum",@"image1",@"image2",@"image3",@"image4",
                     diaryInfo.datetime,
                     diaryInfo.week,
                     diaryInfo.text,
                     diaryInfo.location,
                     diaryInfo.weather,
                     diaryInfo.emotion,
                     diaryInfo.audio,
                     diaryInfo.imgnum,
                     diaryInfo.image1,
                     diaryInfo.image2,
                     diaryInfo.image3,
                     diaryInfo.image4];
    
    
    if(sqlite3_exec(_db, [sql UTF8String], NULL, NULL, &err)!=SQLITE_OK){
        sqlite3_close(_db);
        DebugLog(@"数据库插入数据失败!%@", [NSString stringWithUTF8String:err]);
        return NO;
    }
    
    sqlite3_close(_db);
    
    return YES;
}

/**
 *  更新数据表
 *
 *  @param diaryInfo SaveDiaryInfo对象
 *  @param datetime  更新根据datetime的key
 *
 *  @return 是否成功更新
 */
-(BOOL)updateItem:(SaveDiaryInfo*)diaryInfo WithDatetime:(NSString*)datetime{
    
    if (![self openDbAndCreateSTimeTable]) {
        return NO;
    }
    
    char *err;
    NSString *sql = [NSString stringWithFormat:@"UPDATE '%@' set datetime='%@', week='%@', text='%@', location='%@', weather='%@', emotion='%@', audio='%@', imgnum='%@', image1='%@', image2='%@', image3='%@', image4='%@' where datetime='%@'", TABLENAME,
                     diaryInfo.datetime,
                     diaryInfo.week,
                     diaryInfo.text,
                     diaryInfo.location,
                     diaryInfo.weather,
                     diaryInfo.emotion,
                     diaryInfo.audio,
                     diaryInfo.imgnum,
                     diaryInfo.image1,
                     diaryInfo.image2,
                     diaryInfo.image3,
                     diaryInfo.image4, datetime];
    
    if(sqlite3_exec(_db, [sql UTF8String], NULL, NULL, &err)!=SQLITE_OK){
        sqlite3_close(_db);
        DebugLog(@"数据库更新数据失败!%@", [NSString stringWithUTF8String:err]);
        return NO;
    }
    
    sqlite3_close(_db);
    
    return YES;
}

/**
 *  删除一条日记
 *
 *  @param datetime 根据datetime
 *
 *  @return 是否成功删除
 */
-(BOOL)deleteItem:(NSString*)datetime{
    
    if (![self openDbAndCreateSTimeTable]) {
        return NO;
    }
    
    char *err;
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM '%@' where datetime='%@'", TABLENAME, datetime];
    if(sqlite3_exec(_db, [sql UTF8String], NULL, NULL, &err)!=SQLITE_OK){
        sqlite3_close(_db);
        DebugLog(@"数据库删除数据失败!%@", [NSString stringWithUTF8String:err]);
        return NO;
    }
    
    sqlite3_close(_db);
    
    return YES;
}

/**
 *  第一篇日记到当前时间的天数
 *
 *  @return 总天数字符串，如果失败返回@"0"
 */
-(NSString*)daySum{
    
    NSString *daySum = @"0";
    
    if (![self openDbAndCreateSTimeTable]) {
        return daySum;
    }
    
    NSString *sql = @"SELECT julianday(date('now','+1 day'))-julianday(date(min(datetime))) from listitem_table";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_db, [sql UTF8String], -1, &statement, nil)==SQLITE_OK) {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            int daysum = sqlite3_column_int(statement, 0);          // 天数整数
            daySum = [NSString stringWithFormat:@"%d", daysum];
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(_db);
    
    return daySum;
}

/**
 *  得到照片总数
 *
 *  @return 照片总数，如果失败返回@"0"
 */
-(NSString*)imageSum{
    
    NSString *imageSum = @"0";
    
    if (![self openDbAndCreateSTimeTable]) {
        return imageSum;
    }
    
    NSString *sql = @"SELECT sum(imgnum) from listitem_table";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_db, [sql UTF8String], -1, &statement, nil)==SQLITE_OK) {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            int imagesum = sqlite3_column_int(statement, 0);          // 照片总数
            imageSum = [NSString stringWithFormat:@"%d", imagesum];
        }
        sqlite3_finalize(statement);
    }
    
    sqlite3_close(_db);
    
    return imageSum;
    
}



@end
