//
//  STimeTableDao.h
//  SmartTime
//
//  Created by tsia on 15-6-3.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "SaveDiaryInfo.h"

@interface STimeTableDao : NSObject

-(NSMutableArray*)queryAllItems;
-(BOOL)insertItem:(SaveDiaryInfo*)diaryInfo;
-(BOOL)updateItem:(SaveDiaryInfo*)diaryInfo WithDatetime:(NSString*)datetime;
-(BOOL)deleteItem:(NSString*)datetime;
-(NSString*)daySum;
-(NSString*)imageSum;

@end
