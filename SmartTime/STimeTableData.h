//
//  STimeTableData.h
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface STimeTableData : NSObject

@property (nonatomic, strong)NSMutableArray *listData;
@property (nonatomic, strong)NSString *sumDay;
@property (nonatomic, strong)NSString *sumDiary;
@property (nonatomic, strong)NSString *sumImage;

+(STimeTableData *)sharedInstance;

-(void)freshData;

@end
