//
//  ReadEditViewController.m
//  SmartTime
//
//  Created by tsia on 15-6-6.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "ReadEditViewController.h"
#import "UIColor+Util.h"
#import "DatetimeWeekBarView.h"
#import "SaveDiaryInfo.h"
#import "DateTimeUtil.h"
#import "UITextField+Util.h"
#import "UILabel+Util.h"
#import "STimeTableDao.h"
#import "STimeTableData.h"

#define CARDHEADERVIEW_HEIGHT       (0.5 + 38 + 1)      // 卡片栏高度
#define BOTTOMVIEW_HEIGHT   (0.5 + 38 + 1 + 220)        // 底边区域总高度

#define TEXTVIEW_MARGIN_BOTTOM      6

#define TAG_SAVE    0
#define TAG_EDIT    1

@interface ReadEditViewController ()

@property (nonatomic, strong) SaveDiaryInfo* saveDiary;     // 用于保存或读取的的一条日记数据

@property (nonatomic, strong) UIImageView *ivSaveOrEdit;

@property (nonatomic, strong) DatetimeWeekBarView *bar;
@property (nonatomic, strong) UIImageView *ivEmo;
@property (nonatomic, strong) BottomView *bottom;
@property (nonatomic, strong) UITextView *textfield;
@property (nonatomic, strong) UIScrollView *scrollview;
@property (nonatomic, strong) UIImageView *ivloc;   // 位置图片和文字
@property (nonatomic, strong) UILabel *lbloc;
@property (nonatomic, strong) UILabel *lbtime;

@property (nonatomic, strong) UIImagePickerController *picker;
@property (nonatomic, strong) NSMutableArray *arrUiimgspath;   // 保存UI显示顺序的图片路径数组
@property (nonatomic, strong) NSArray *arrEmoImages;
@property (nonatomic, strong) NSDictionary *dictWeaText;
@property (nonatomic, assign) BOOL isKyboardShown;  // 键盘是否显示

@end

@implementation ReadEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initData];
    [self initUI];

}

-(void)initData{
    
    _arrEmoImages = @[@"emo_1", @"emo_2",@"emo_3",@"emo_4",@"emo_5",@"emo_6",@"emo_7",@"emo_8",@"emo_9",@"emo_10",@"emo_11",@"emo_12"];
    _dictWeaText = @{@"晴":@"0", @"多云":@"1", @"阴":@"2", @"小雨":@"3", @"中雨":@"4", @"暴雨":@"5", @"雷雨":@"6", @"小雪":@"7"};
    
    _saveDiary = [[SaveDiaryInfo alloc]init];
    _saveDiary.image1 = @"";    // 若没有图片，默认@""
    _saveDiary.image2 = @"";
    _saveDiary.image3 = @"";
    _saveDiary.image4 = @"";
    
    _picker = [[UIImagePickerController alloc]init];
    UIImagePickerControllerSourceType sourcheType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    _picker.sourceType = sourcheType;
    _picker.delegate = self;
    _picker.allowsEditing = YES;
  
    _arrUiimgspath = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", nil];
    
    _ivloc = [[UIImageView alloc]initWithFrame:CGRectZero];
    _lbloc = [[UILabel alloc]initWithFrame:CGRectZero];
    _lbtime = [[UILabel alloc]initWithFrame:CGRectZero];
}

-(void)initNavi{
    
    // 导航栏右侧图标，编辑或保存
    _ivSaveOrEdit = [CustomNaviBarView createNaviBarImageView:@"savepage" target:self action:@selector(tapSaveOrEdit:)];
    _ivSaveOrEdit.tag = TAG_SAVE;
    [self setNaviBarRightFirstIv:_ivSaveOrEdit];
}

-(void)initUI{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initNavi];
    
    // 圆形头像
    UIImageView *ivHead = [[UIImageView alloc]initWithFrame:Rect(4, StatusBarHeight+STimeNaviBarHeight+4, 48, 48)];
    ivHead.image = [UIImage imageNamed:@"head.jpg"];
    ivHead.contentMode = UIViewContentModeScaleAspectFill;
    ivHead.layer.masksToBounds = YES;
    ivHead.layer.cornerRadius = 24;
    [self.view addSubview:ivHead];
    
    // 底部的分割线
    UIImageView *ivDevider = [[UIImageView alloc]initWithFrame:Rect(4+48, CGRectGetMaxY(ivHead.frame)-1, ScreenWidth-CGRectGetMidX(ivHead.frame), 1)];
    ivDevider.image = [UIImage imageNamed:@"list_divider.png"];
    [self.view addSubview:ivDevider];
    
    // 录音
    UIImageView *recordaudio = [[UIImageView alloc]initWithFrame:Rect(ScreenWidth-14-35, StatusBarHeight+STimeNaviBarHeight+5, 35, 35)];
    recordaudio.image = [UIImage imageNamed:@"record_audio.png"];
    recordaudio.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:recordaudio];
    
    // 录音和表情的分隔线
    UIView *imgDevider = [[UIImageView alloc]initWithFrame:Rect(ScreenWidth-14-35-14-1, StatusBarHeight+STimeNaviBarHeight+10, 1, 30)];
    imgDevider.backgroundColor = [UIColor colorFromRGB:0x878787];
    [self.view addSubview:imgDevider];
    
    // 表情
    _ivEmo = [[UIImageView alloc]initWithFrame:Rect(CGRectGetMinX(imgDevider.frame)-14-30, StatusBarHeight+STimeNaviBarHeight+9, 30, 30)];
    //_ivEmo.image = [UIImage imageNamed:@"emo_1.png"];
    _ivEmo.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_ivEmo];
    
    // 日期条，并初始化显示时间
    _bar = [[DatetimeWeekBarView alloc]initWithWidth:CGRectGetMinX(_ivEmo.frame)-CGRectGetMaxX(ivHead.frame)-12];
    NSString *bardatetime = self.datetimekey==nil?[DateTimeUtil nowTime]:self.datetimekey;
    _bar.datetime.text = [DateTimeUtil dateFromDateTime:bardatetime];
    _bar.week.text = [DateTimeUtil weekFromDateTime:bardatetime];
    _bar.frame = Rect(CGRectGetMaxX(ivHead.frame)+12, StatusBarHeight+STimeNaviBarHeight+(48-_bar.frameSize.height)/2 +6, _bar.frameSize.width, _bar.frameSize.height);
    [self.view addSubview:_bar];
    
    
    CGFloat ystart = CGRectGetMaxY(ivHead.frame);
    // 滚动视图
    _scrollview = [[UIScrollView alloc]initWithFrame:Rect(0, ystart+12, ScreenWidth, SelfViewHeight-ystart-12)];
    [self.view addSubview:_scrollview];
    
    // 文本区
    _textfield = [[UITextView alloc]initWithFrame:Rect(12, 0, ScreenWidth - 12*2, 0)];
    _textfield.font = [UIFont fontWithName:@"HelveticaNeue" size:16.0f];
    [_textfield becomeFirstResponder];   // 成为第一响应者，触发键盘弹出
    [_scrollview addSubview:_textfield];
    
    // 底边卡片区
    _bottom = [[BottomView alloc]initWithFrame:Rect(0, SelfViewHeight-BOTTOMVIEW_HEIGHT, ScreenWidth, BOTTOMVIEW_HEIGHT)];
    _bottom.controllerDelegate = self;
    _bottom.dataDelegate = self;
    [self.view addSubview:_bottom];
    
    
    if (self.datetimekey != nil) {
        // 阅读模式初始化
        [self initReadMode];
    }
    
}

-(void)initReadMode{
    
    /**
     *  得到本条日记数据
     */
    _saveDiary = [STimeTableData sharedInstance].listData[self.rowidx];
    
    // 按钮变为编辑
    _ivSaveOrEdit.image = [UIImage imageNamed:@"editpage.png"];
    _ivSaveOrEdit.tag = TAG_EDIT;
    
    // 初始化日期条
    _bar.datetime.text = [DateTimeUtil dateFromDateTime:_saveDiary.datetime];
    _bar.week.text = [DateTimeUtil weekFromDateTime:_saveDiary.datetime];
    if (IS_DBTEXT_NOTEMP(_saveDiary.weather)) {
        _bar.weather.text = _saveDiary.weather;
    }
    
    // 初始化表情
    if (IS_DBTEXT_NOTEMP(_saveDiary.emotion)) {
        int index = [_saveDiary.emotion intValue];
        _ivEmo.image = [UIImage imageNamed:_arrEmoImages[index]];
    }
    
    // 初始化卡片栏和四张卡片
    // location
    if (IS_DBTEXT_NOTEMP(_saveDiary.location)) {
        [_bottom.cardHead setHeaderIndex:0 Green:YES];  // 按钮绿显
        [_bottom.cardsView.locationCard updateLocation:_saveDiary.location];
    }
    // photo
    if (IS_DBIMGNUM_NOTZERO(_saveDiary.imgnum)) {
        [_bottom.cardHead setHeaderIndex:1 Green:YES];  // 按钮绿显
        NSArray *arrimg = @[_saveDiary.image1, _saveDiary.image2, _saveDiary.image3, _saveDiary.image4];
        [_bottom.cardsView.photoCard showImageWithPathArry:arrimg];
    }
    // weather
    if (IS_DBTEXT_NOTEMP(_saveDiary.weather)) {
        [_bottom.cardHead setHeaderIndex:2 Green:YES];  // 按钮绿显
        int index = [[_dictWeaText objectForKey:_saveDiary.weather] intValue];
        [_bottom.cardsView.weatherCard setBordeViewFrameWithIndex:index];   // 选中边框显示
    }
    // emotion
    if (IS_DBTEXT_NOTEMP(_saveDiary.emotion)) {
        [_bottom.cardHead setHeaderIndex:3 Green:YES];  // 按钮绿显
        int index = [_saveDiary.emotion intValue];
        [_bottom.cardsView.emotionCard setBordeViewFrameWithIndex:index];   // 选中边框显示
    }
    
    // 隐藏底边栏
    _bottom.hidden = YES;
    
    // TextField不可编辑
    [_textfield resignFirstResponder];
    _textfield.editable = NO;
    
    CGFloat contentheight = 0.f;
    // 初始化文字
    if (IS_DBTEXT_NOTEMP(_saveDiary.text)) {
        _textfield.text = _saveDiary.text;
        CGFloat textheight = [_textfield textAreaSizeWithWidth:ScreenWidth - 12*2].height +16;
        _textfield.frame = RectSetHeight(_textfield.frame, textheight);
        contentheight += textheight;
    }
    
    // 初始化图片区
    if (IS_DBIMGNUM_NOTZERO(_saveDiary.imgnum)) {
        [_bottom.cardsView.photoCard removeFromSuperview];
        _bottom.cardsView.photoCard.frame = Rect(0, contentheight, SelfViewWidth, 220);
        [_scrollview addSubview:_bottom.cardsView.photoCard];
        _bottom.cardsView.photoCard.userInteractionEnabled = NO;    // 不响应点击事件，子view也不响应
        contentheight += 220;
    }
    
    // 初始化地点条数据
    // 地点条
    if (IS_DBTEXT_NOTEMP(_saveDiary.location)) {
        // 地点图标
        _ivloc.frame = Rect(6, contentheight, 16, 19);
        _ivloc.image = [UIImage imageNamed:@"read_location.png"];
        _ivloc.contentMode = UIViewContentModeScaleAspectFit;
        [_scrollview addSubview:_ivloc];
        
        // 地点文字
        _lbloc.text = _saveDiary.location;
        CGSize locSize = [_lbloc textAreaSizeWithWidth:ViewWidth(_scrollview)-ViewWidth(_ivloc)];
        _lbloc.frame = Rect(6+ViewWidth(_ivloc), contentheight, locSize.width, locSize.height);
        [_scrollview addSubview:_lbloc];
        contentheight += locSize.height;
    }
    
    // 时刻
    _lbtime.text = [DateTimeUtil timeFromDateTime:_saveDiary.datetime];  // 阅读第一次打开显示日记中的时间
    _lbtime.textColor = [UIColor colorFromRGB:0x888888];
    CGSize timeSize = [_lbtime textAreaSize];
    _lbtime.frame = Rect(9, 6+contentheight, timeSize.width, timeSize.height);
    [_scrollview addSubview:_lbtime];
    contentheight += 6+timeSize.height;
    
    // 整个滚动视图内容的size
    _scrollview.contentSize = Size(ViewWidth(_scrollview), contentheight + 12);
    
}

// 注册键盘显示和隐藏事件监听
-(void)registerForKeyboardNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [self registerForKeyboardNotifications];
}

-(void)keyboardWasShown:(NSNotification*)aNotification{
    _isKyboardShown = YES;
    
    NSDictionary* info = [aNotification userInfo];
    //kbSize即为键盘尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSLog(@"hight_hitht:%f",kbSize.height);
    
    [self showBottomViewWithKeyboard:kbSize.height IsKeyboardShow:YES];
    
    [self adjustTextFieldHeightWithKeyboard:kbSize.height IsKeyboardShow:YES];

}
-(void)keyboardWillBeHidden:(NSNotification*)aNotification{
    _isKyboardShown = NO;
    
    if (_bottom.isHidden) {
        // 阅读模式
        return;
    }
    [self showBottomViewWithKeyboard:0 IsKeyboardShow:NO];
    [self adjustTextFieldHeightWithKeyboard:0 IsKeyboardShow:NO];
}

// 根据键盘显示高度设置BottomView
-(void)showBottomViewWithKeyboard:(CGFloat)kbheight IsKeyboardShow:(BOOL)bshown{
    
    if (_bottom.hidden) {
        _bottom.hidden = NO;
    }
    
    [UIView transitionWithView:_bottom duration:0.2 options:UIViewAnimationOptionTransitionNone animations:^{
        
        //UIView属性改变的最终状态
        CGRect frame = _bottom.frame;
        frame.origin.y = bshown?SelfViewHeight-CARDHEADERVIEW_HEIGHT-kbheight:SelfViewHeight-BOTTOMVIEW_HEIGHT;
        _bottom.frame = frame;
        
    } completion:nil];
    
}

// 根据键盘显示高度调整textView高度
-(void)adjustTextFieldHeightWithKeyboard:(CGFloat)kbheight IsKeyboardShow:(BOOL)bshown{
    
    [UIView transitionWithView:_textfield duration:0.2 options:UIViewAnimationOptionTransitionNone animations:^{
        
        //UIView属性改变的最终状态
        CGRect frame = _textfield.frame;
        frame.size.height = bshown?(SelfViewHeight-RectY(_scrollview.frame)-CARDHEADERVIEW_HEIGHT-kbheight-TEXTVIEW_MARGIN_BOTTOM):(SelfViewHeight-RectY(_scrollview.frame)-BOTTOMVIEW_HEIGHT-TEXTVIEW_MARGIN_BOTTOM);
        _textfield.frame = frame;
        
    } completion:nil];
    
    // 然后滚动到最后一行，并且光标定位到最后一个字
    [_textfield scrollRangeToVisible:NSMakeRange(_textfield.text.length, 1)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 保存或编辑
-(void)tapSaveOrEdit:(UIPanGestureRecognizer*)recognizer{
    
    if (_ivSaveOrEdit.tag==TAG_SAVE) {
        /**
         *
         * id INTEGER (primary key autoincrement)
         * datetime     TEXT
         * week     TEXT
         * text     TEXT
         * location TEXT
         * weather  TEXT
         * emotion TEXT  表情
         * audio   TEXT  录音
         * imgnum  TEXT  照片数量
         * image1  TEXT  照片1的路径
         * image2  TEXT  照片2的路径
         * image3  TEXT  照片3的路径
         * image4  TEXT  照片4的路径
         *
         */
        
        
        _saveDiary.text = _textfield.text;
        // location已保存，默认nil
        // weather已保存，默认nil
        // emotion以保存，默认nil
        
        // audio
        
        _saveDiary.imgnum = [NSString stringWithFormat:@"%i", [self imageNum]];
        
        // 把_arrUiimgspath中的路径排序保存到_saveDiary
        NSArray *arrSorted = [self sortUiImagePaths];
        _saveDiary.image1 = arrSorted[0];
        _saveDiary.image2 = arrSorted[1];
        _saveDiary.image3 = arrSorted[2];
        _saveDiary.image4 = arrSorted[3];
        
        /**
         *  阅读模式的UI
         */
        
        // 隐藏底边栏
        _bottom.hidden = YES;
        
        // 显示地点条和time
        _ivloc.hidden = NO;
        _lbloc.hidden = NO;
        _lbtime.hidden = NO;
        
        // TextField不可编辑
        [_textfield resignFirstResponder];
        _textfield.editable = NO;
        
        CGFloat contentheight = 0.f;
        
        if (IS_DBTEXT_NOTEMP(_saveDiary.text)) {
            // 重新计算TextField的高度，为所有文本的高度
            CGFloat textheight = [_textfield textAreaSizeWithWidth:ScreenWidth - 12*2].height +16;
            _textfield.frame = RectSetHeight(_textfield.frame, textheight);
            contentheight += textheight;
        }
        
        // 图片区
        if (![_saveDiary.imgnum isEqualToString:@"0"]) {
            [_bottom.cardsView.photoCard removeFromSuperview];
            _bottom.cardsView.photoCard.frame = Rect(0, contentheight, SelfViewWidth, 220);
            [_scrollview addSubview:_bottom.cardsView.photoCard];
            _bottom.cardsView.photoCard.userInteractionEnabled = NO;    // 不响应点击事件，子view也不响应
            contentheight += 220;
        }
        
        // 地点条
        if (IS_DBTEXT_NOTEMP(_saveDiary.location)) {
            // 地点图标
            _ivloc.frame = Rect(6, contentheight, 16, 19);
            _ivloc.image = [UIImage imageNamed:@"read_location.png"];
            _ivloc.contentMode = UIViewContentModeScaleAspectFit;
            [_scrollview addSubview:_ivloc];
            
            // 地点文字
            _lbloc.text = _saveDiary.location;
            CGSize locSize = [_lbloc textAreaSizeWithWidth:ViewWidth(_scrollview)-ViewWidth(_ivloc)];
            _lbloc.frame = Rect(6+ViewWidth(_ivloc), contentheight, locSize.width, locSize.height);
            [_scrollview addSubview:_lbloc];
            contentheight += locSize.height;
        }else{
            _ivloc.hidden = YES;
            _lbloc.hidden = YES;
        }
        
        // 时刻
        _lbtime.text = [DateTimeUtil timeFromDateTime:[DateTimeUtil nowTime]];      // 无论何种情况，保存时总显示当前时间
        _lbtime.textColor = [UIColor colorFromRGB:0x888888];
        CGSize timeSize = [_lbtime textAreaSize];
        _lbtime.frame = Rect(9, 6+contentheight, timeSize.width, timeSize.height);
        [_scrollview addSubview:_lbtime];
        contentheight += 6+timeSize.height;
        
        // 整个滚动视图内容的size
        _scrollview.contentSize = Size(ViewWidth(_scrollview), contentheight + 12);
        
        
        /**
         *  保存数据到数据表中
         */
        _saveDiary.datetime = [DateTimeUtil nowTime];   // 写入数据库的时间
        _saveDiary.week = [DateTimeUtil weekFromDateTime:_saveDiary.datetime];
        
        STimeTableDao *dao = [[STimeTableDao alloc]init];
        if (self.datetimekey == nil) {
            // 创建模式下第一次保存
            [dao insertItem:_saveDiary];
        }else{
            // 已经保存过一次，后续保存
            [dao updateItem:_saveDiary WithDatetime:self.datetimekey];   // 会更新datetime为保存的时间
        }
        self.datetimekey = _saveDiary.datetime;
        
        
        // 按钮变为编辑
        _ivSaveOrEdit.image = [UIImage imageNamed:@"editpage.png"];
        _ivSaveOrEdit.tag = TAG_EDIT;
        
    }else{
        // 编辑按钮的点击事件
        
        // 转移图片区
        if (![_saveDiary.imgnum isEqualToString:@"0"]) {
            [_bottom.cardsView.photoCard removeFromSuperview];
            _bottom.cardsView.photoCard.frame = Rect(SelfViewWidth, 0, SelfViewWidth, 220);
            [_bottom.cardsView addSubview:_bottom.cardsView.photoCard];
            _bottom.cardsView.photoCard.userInteractionEnabled = YES;   // 响应点击
        }
        
        // 隐藏地点条和time
        _ivloc.hidden = YES;
        _lbloc.hidden = YES;
        _lbtime.hidden = YES;
        
        // TextField可编辑
        _textfield.editable = YES;
        [_textfield becomeFirstResponder];
        
        
        // 按钮变为保存
        _ivSaveOrEdit.image = [UIImage imageNamed:@"savepage.png"];
        _ivSaveOrEdit.tag = TAG_SAVE;
    }
    
}

-(int)imageNum{
    int num = 0;
    for (NSString *str in _arrUiimgspath) {
        if (str.length>0) {
            num++;
        }
    }
    return num;
}
-(NSArray*)sortUiImagePaths{

    NSMutableArray *arr = [NSMutableArray arrayWithObjects:@"", @"", @"", @"", nil];
    int j = 0;
    for (int i=0; i<4; i++) {
        NSString *path = _arrUiimgspath[i];
        if (path.length>0) {
            arr[j++] = path;
        }
    }
    return arr;
}

#pragma mark - CardHeaderControllerProtocol
-(void)hideKeyboard{
    [_textfield resignFirstResponder];  // 会触发键盘隐藏事件
}
-(BOOL)isKeyboardShown{
    return  _isKyboardShown;
}
-(void)requireLocation{
    
    [self setNaviBarTitle:@"正在获取..."];
    // 设置其代理
    [LocationService sharedInstance].delegate = self;
    [[LocationService sharedInstance] startLocate];
}

#pragma mark - LocationServiceDoUI
-(void)updateLocationForUI:(NSString *)location IsSuccess:(BOOL)issucceed{
    
    if (issucceed) {
        // 更新显示
        [_bottom updateLocationCard:location];
        // 保存数据
        _saveDiary.location = location;
    }
    
    [self setNaviBarTitle:nil];  
}

#pragma mark - CardsDataControllerDelegate
-(void)setWeather:(NSString *)weatext{
    _saveDiary.weather = weatext;
    _bar.weather.text = weatext;
}
-(void)setEmo:(NSString *)index{
    _saveDiary.emotion = index;
    
    // 如果清空的话传nil，否则是0-11字符串
    if (index == nil) {
        _ivEmo.image = nil;
    }else{
        _ivEmo.image = [UIImage imageNamed:_arrEmoImages[[index intValue]]];
    }
}
-(void)setlocation:(NSString *)location{
    _saveDiary.location = location;
}
-(void)setPhoto:(NSString *)path WithIndex:(NSInteger)index{
    
    _arrUiimgspath[index] = path;
}

// 弹出选择窗口
-(void)presentImagePicker{
    [self presentViewController:_picker animated:YES completion:nil];
}
// 选取成功
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"%@", path);

    // 退出选择窗口
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    [_bottom.cardsView.photoCard readLocalImage:path];
}

@end
