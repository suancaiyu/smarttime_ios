//
//  CustomNaviBarView.m
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CustomNaviBarView.h"

@interface CustomNaviBarView()

@property (nonatomic, readonly) UIImageView *ivFirstLeft;
@property (nonatomic, readonly) UIImageView *ivSecondLeft;
@property (nonatomic, readonly) UIImageView *ivRightFirst;
@property (nonatomic, readonly) UIImageView *ivRightSecond;
@property (nonatomic, readonly) UILabel *lbtitle;

@end

@implementation CustomNaviBarView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    
    return self;
}

// 创建一个导航栏上的按钮图标，并没有设置frame
+(UIImageView*)createNaviBarImageView:(NSString*)img target:(id)target action:(SEL)action{
    
    UIImageView *iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:img]];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    iv.userInteractionEnabled = YES;
    UITapGestureRecognizer *singletap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    [iv addGestureRecognizer:singletap];
    
    return iv;
}

-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    /**
     *  使用系统的毛玻璃效果。
     *
     *  如果没有默认导航栏高度44，必须要重写UINavigationBar的layoutSubviews
     */
    UINavigationBar *naviBar = [[UINavigationBar alloc] initWithFrame:self.bounds];
    //naviBar.translucent = YES;
    naviBar.barStyle = UIBarStyleBlack;
    naviBar.alpha = 0.8;
    [self addSubview:naviBar];
    
    // 默认显示返回图标，默认的动作为弹出当前控制器
    _ivFirstLeft = [[self class] createNaviBarImageView:@"backtomain" target:self action:@selector(btnBack:)];

    
    // 标题
    _lbtitle = [[UILabel alloc]initWithFrame:CGRectZero];
    _lbtitle.backgroundColor = [UIColor clearColor];
    _lbtitle.textColor = [UIColor whiteColor];
    _lbtitle.font = [UIFont fontWithName:@"HelveticaNeue" size:20];
    _lbtitle.textAlignment = NSTextAlignmentCenter;
    

    _lbtitle.frame = CGRectMake(0, 20, ScreenWidth, STimeNaviBarHeight);

    
    [self addSubview:_lbtitle];
    
    [self setLeftFirstImageView:_ivFirstLeft];
    
}

-(void)setLeftFirstImageView:(UIImageView*)iv{
    
    if (_ivFirstLeft) {
        [_ivFirstLeft removeFromSuperview];
        _ivFirstLeft = nil;
    }
    
    _ivFirstLeft = iv;
    if (_ivFirstLeft) {
        _ivFirstLeft.frame = Rect(8.f, StatusBarHeight+9.f, 30, 30);   // 设置frame
        [self addSubview:_ivFirstLeft];
    }
}

-(void)setLeftSecondImageView:(UIImageView*)iv{
    
    if (_ivSecondLeft) {
        [_ivSecondLeft removeFromSuperview];
        _ivSecondLeft = nil;
    }
    
    _ivSecondLeft = iv;
    if (_ivSecondLeft) {
        _ivSecondLeft.frame = Rect(8+28+8, StatusBarHeight+11.f, 120, 30);
        [self addSubview:_ivSecondLeft];
    }
}

-(void)setRightFirstImage:(UIImageView*)iv{
    
    if (_ivRightFirst) {
        [_ivRightFirst removeFromSuperview];
        _ivRightFirst = nil;
    }
    
    _ivRightFirst = iv;
    if (_ivRightFirst) {
        _ivRightFirst.frame = Rect(ScreenWidth-15-30, StatusBarHeight+9.f, 30, 30);
        [self addSubview:_ivRightFirst];
    }
}

-(void)setRightSecondImage:(UIImageView*)iv{
    
    if (_ivRightSecond) {
        [_ivRightSecond removeFromSuperview];
        _ivRightSecond = nil;
    }
    
    _ivRightSecond = iv;
    if (_ivRightSecond) {
        _ivRightSecond.frame = Rect(ScreenWidth-15-30- 18-30, StatusBarHeight+9.f, 30, 30);
        [self addSubview:_ivRightSecond];
    }
}

- (void)setTitle:(NSString *)title{
    [_lbtitle setText:title];
}

- (void)btnBack:(id)sender{
    
    if (_parentViewController){
        [_parentViewController.navigationController popViewControllerAnimated:YES];
    }
}

@end
