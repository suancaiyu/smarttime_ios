//
//  STimeCellView.m
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "STimeCellView.h"
#import "CellBubbleView.h"

#define DIVIDERLINE_HEIGHT      1

#define TIMELINE_WIDTH      22      // 时光轴线宽度，固定22pt

#define BUBBLE_MARGIN_TOP     10
#define BUBBLE_MARGIN_RIGHT     10
#define BUBBLE_MARGIN_BOTTOM     10

#define LEFTVIEW_BOTTOM_SPACE   20  // 决定单元高度时，日期区域加上的高度

@interface STimeCellView(){          //这是匿名分类
    
    CGFloat _width;  //父视图给的宽度
    NSInteger _index;
}
@end

@implementation STimeCellView

-(id)initWithIndex:(NSInteger)index{
    
    self = [super init];
    
    if (self) {
        
        _width = ScreenWidth;
        _index = index;
        
        [self initUI];
    }
    
    return self;
}

-(void)initUI{
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    // 顶部的分割线
    UIImageView *ivdivide = [[UIImageView alloc]initWithFrame:Rect(0, 0, _width, DIVIDERLINE_HEIGHT)];
    ivdivide.image = [UIImage imageNamed:@"list_divider.png"];
    [self addSubview:ivdivide];
    
    // 日期区
    _leftView = [[CellLeftView alloc]initWithIndex:_index];
    [_leftView setFrame:Rect(0, DIVIDERLINE_HEIGHT, _leftView.framesize.width, _leftView.framesize.height)];
    [self addSubview:_leftView];
    
    // 右边的视图
    CGFloat bubblewidth = _width - _leftView.frame.size.width - TIMELINE_WIDTH - BUBBLE_MARGIN_RIGHT;//bubbleView的宽度
    _bubbleView = [[CellBubbleView alloc]initWithWidth:bubblewidth Index:_index];
    _bubbleView.frame = Rect(_leftView.frame.size.width+TIMELINE_WIDTH, BUBBLE_MARGIN_TOP, bubblewidth, _bubbleView.bubbleheight);
    [self addSubview:_bubbleView];
    
    // 比较左右视图的高度，决定整个单元视图的高度
    CGFloat leftheight = _leftView.framesize.height+LEFTVIEW_BOTTOM_SPACE;
    CGFloat rightheight = BUBBLE_MARGIN_TOP + _bubbleView.bubbleheight + BUBBLE_MARGIN_BOTTOM;// 右边整个视图的高度
    
    _cellheight = leftheight>rightheight?leftheight:rightheight;
    
    // 时光轴区 (此时再设置时光轴线的高度)
    _timeLineView = [[TimeLineView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_leftView.frame), DIVIDERLINE_HEIGHT, TIMELINE_WIDTH, _cellheight)];
    [self addSubview:_timeLineView];
}

@end
