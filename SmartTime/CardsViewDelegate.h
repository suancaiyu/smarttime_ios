//
//  CardsViewDelegate.h
//  SmartTime
//
//  Created by tsia on 15-6-10.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

/**
 *  四张卡片的公用代理，由BottomView来实现
 */

#ifndef SmartTime_CardsViewDelegate_h
#define SmartTime_CardsViewDelegate_h


@protocol CardHearColorDelegate <NSObject>

/**
 *  设置卡片栏按钮
 *
 *  @param index   卡片栏索引
 *  @param isGreen 是否绿显
 */
-(void)setCardHeadColor:(NSInteger)index Green:(BOOL)isGreen;

@end


/**
 *  卡片事件需要调用控制器方法更新数据，ReadEditController实现方法
 */
@protocol CardsDataControllerDelegate <NSObject>

-(void)setPhoto:(NSString*)path WithIndex:(NSInteger)index;
-(void)setlocation:(NSString*)location;
-(void)setWeather:(NSString*)weatext;
-(void)setEmo:(NSString*)index; //0-11字符串

-(void)presentImagePicker;

@end


#endif
