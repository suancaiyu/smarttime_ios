//
//  UITextField+Util.m
//  SmartTime
//
//  Created by tsia on 15-6-12.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "UITextField+Util.h"

@implementation UITextView(Util)

-(CGSize)textAreaSizeWithWidth:(CGFloat)width{
    
    return [self.text boundingRectWithSize:CGSizeMake(width, 0) options:NSStringDrawingTruncatesLastVisibleLine |
            NSStringDrawingUsesLineFragmentOrigin |
            NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObjectsAndKeys:self.font, NSFontAttributeName, nil] context:nil].size;
}

@end
