//
//  PhotoCardView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "PhotoCardView.h"
#import "LocalImageUtil.h"

#define PHOTO_GAP   4
#define PHOTO_MARGIN_SIDE   8

@interface PhotoCardView(){
    
    NSInteger _curClick;
}

@end

@implementation PhotoCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self initUI];
    }
    return self;
}

-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    CGFloat photowidth = (ViewWidth(self)-PHOTO_GAP-PHOTO_MARGIN_SIDE*2)/2;
    CGFloat photoheight = (ViewHeight(self)-PHOTO_GAP-PHOTO_MARGIN_SIDE*2)/2;
    UIImage *imgadd = [UIImage imageNamed:@"photo_add.png"];
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithCapacity:4];
    
    for (int i=0; i<2; i++) {
        for (int j=0; j<2; j++) {
            NSInteger idx = 2*i + j;
            
            UIImageView *iv = [[UIImageView alloc]initWithImage:imgadd];
            iv.tag = idx;
            iv.contentMode = UIViewContentModeScaleToFill;
            UITapGestureRecognizer *recognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
            UILongPressGestureRecognizer *lrecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handlerLongPressGesture:)];
            iv.userInteractionEnabled = YES;
            [iv addGestureRecognizer:recognizer];
            [iv addGestureRecognizer:lrecognizer];
            iv.frame = Rect(PHOTO_MARGIN_SIDE+(photowidth+PHOTO_GAP)*j, PHOTO_MARGIN_SIDE+(photoheight+PHOTO_GAP)*i, photowidth, photoheight);
            [self addSubview:iv];
            [arr addObject:iv];
        }
    }
    
    _photoIvs = [[NSArray alloc]initWithArray:arr];
    
}

// 单击事件
-(void)handlerTapGesture:(UITapGestureRecognizer*)recognizer{
    
    NSLog(@"handlerTapGesture");
    
    NSInteger tag = recognizer.view.tag;
    // 当前点击的图片控件索引
    _curClick = tag%10;
    
    [_pickerDelegate presentImagePicker];
}
// 长按事件
-(void)handlerLongPressGesture:(UILongPressGestureRecognizer*)lrecognizer{
    
    if (lrecognizer.state == UIGestureRecognizerStateEnded) {
        
        NSInteger tag = lrecognizer.view.tag;
        if (tag<10) {   // 原来就没有图片不处理
            return;
        }
        
        tag = lrecognizer.view.tag%10;
        UIImageView *iv = _photoIvs[tag];
        
        iv.contentMode = UIViewContentModeScaleToFill;
        iv.image = [UIImage imageNamed:@"photo_add.png"]; // 显示添加图片
        
        [_pickerDelegate setPhoto:@"" WithIndex:tag];   // 调用代理清空数据，使用@""，而不是nil
        iv.tag = tag;   // 清除图片后，tag回到0123
        
        [_headerDelegte setCardHeadColor:1 Green:[self isHasPhoto]];    // 绿显或灰显
    }
}

/**
 *  得到了选取的图片的url，调用该方法显示和更新数据
 *
 *  @param path assets-library://asset/asset.JPG?id=224D9935-3252-4E8E-95D8-FEFB834A5DEE&ext=JPG
 */
-(void)readLocalImage:(NSURL*)path{
    
    UIImageView *iv = _photoIvs[_curClick];
    iv.contentMode = UIViewContentModeScaleAspectFill;
    iv.clipsToBounds = YES;
    
    // 图片显示
    [LocalImageUtil readImageFromLocalPath:path ToImageView:iv];
    
    // 使用代理更新数据
    [_pickerDelegate setPhoto:[path absoluteString] WithIndex:_curClick];
    // 图片选择成功后，tag+10
    iv.tag = _curClick+10;
    
    // 绿显或灰显
    [_headerDelegte setCardHeadColor:1 Green:[self isHasPhoto]];
}

// 判断是否至少有一张图片
-(BOOL)isHasPhoto{
    
    NSInteger tagsum = 0;
    for (UIView *v in _photoIvs) {
        tagsum += v.tag;
    }
    
    if (tagsum<10) {
        return NO;
    }
    return YES;
}

// 初始化时显示图片，主要被外部调用
-(void)showImageWithPathArry:(NSArray*)arrimg{
    
    NSString *path;
    UIImageView *iv;
    for (int i=0; i<4; i++) {
        path = arrimg[i];
        if (IS_DBIMGNUM_NOTZERO(path)) {
            iv = _photoIvs[i];
            [LocalImageUtil readImageFromLocalPath:[NSURL URLWithString:path] ToImageView:iv];
        }
    }
}

@end
