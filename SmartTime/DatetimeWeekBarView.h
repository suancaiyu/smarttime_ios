//
//  DatetimeWeekBarView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatetimeWeekBarView : UIView

@property (nonatomic, assign) CGSize frameSize;

@property (nonatomic, strong) UILabel *datetime;
@property (nonatomic, strong) UILabel *week;
@property (nonatomic, strong) UILabel *weather;

-(instancetype)initWithWidth:(CGFloat)givenWidth;

@end
