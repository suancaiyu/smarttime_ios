//
//  STimeViewController.h
//  SmartTime
//
//  Created by tsia on 15/5/29.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomViewController.h"

@interface STimeViewController : CustomViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)UITableView *stimetable;



@end

