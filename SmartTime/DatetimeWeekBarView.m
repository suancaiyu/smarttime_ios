//
//  DatetimeWeekBarView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "DatetimeWeekBarView.h"
#import "UIColor+Util.h"
#import "UILabel+Util.h"

@interface DatetimeWeekBarView(){
    
    CGFloat _fontSize;
}

@end

@implementation DatetimeWeekBarView

-(instancetype)initWithWidth:(CGFloat)givenWidth
{
    self = [super init];
    if (self) {
        
        _fontSize = 16.f;   // 初始默认16.0f
        [self initUI];
        [self fitTextSizeWithWidth:givenWidth];
    }
    return self;
}

-(void)initUI{
    
    // 日期
    _datetime = [[UILabel alloc]initWithFrame:CGRectZero];
    _datetime.text = @"2015-06-07";
    _datetime.font = [UIFont fontWithName:@"HelveticaNeue" size:_fontSize];
    _datetime.textColor = [UIColor colorFromRGB:0x008b00];
    CGSize datetimeSize = [_datetime textAreaSize];
    _datetime.frame = Rect(0, 0, datetimeSize.width, datetimeSize.height);
    [self addSubview:_datetime];
    
    // 星期
    _week = [[UILabel alloc]initWithFrame:CGRectZero];
    _week.text = @"周日";
    _week.font = [UIFont fontWithName:@"HelveticaNeue" size:_fontSize];
    _week.textColor = [UIColor colorFromRGB:0x008b00];
    CGSize weekSize = [_week textAreaSize];
    _week.frame = Rect(CGRectGetMaxX(_datetime.frame)+12, 0, weekSize.width, weekSize.height);
    [self addSubview:_week];
    
    // 天气
    _weather = [[UILabel alloc]initWithFrame:CGRectZero];
    _weather.text = @"多云"; // 占位
    _weather.font = [UIFont fontWithName:@"HelveticaNeue" size:_fontSize];
    _weather.textColor = [UIColor colorFromRGB:0x008b00];
    CGSize weatherSize = [_weather textAreaSize];
    _weather.frame = Rect(CGRectGetMaxX(_week.frame)+12, 0, weatherSize.width, weatherSize.height);
    _weather.text = nil;
    [self addSubview:_weather];
    
    _frameSize = CGSizeMake(datetimeSize.width+12+weekSize.width+12+weatherSize.width, weatherSize.height);
    
}

/**
 *  根据给定宽度调整字体大小
 *
 *  @param givenwidth 给定宽度
 */
-(void)fitTextSizeWithWidth:(CGFloat)givenwidth{
    
    while (_frameSize.width>givenwidth) {
        _fontSize--;
        
        // 先移除之前的子view
        [[self subviews]makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self initUI];
    }
}

@end
