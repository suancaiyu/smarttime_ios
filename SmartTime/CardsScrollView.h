//
//  CardsScrollView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "LocationCardView.h"
#import "PhotoCardView.h"
#import "WeatherCardView.h"
#import "EmotionCardView.h"

@interface CardsScrollView : UIScrollView

@property (nonatomic, assign) NSInteger curIndex;
@property (nonatomic, strong) LocationCardView *locationCard;
@property (nonatomic, strong) PhotoCardView *photoCard;
@property (nonatomic, strong) WeatherCardView *weatherCard;
@property (nonatomic, strong) EmotionCardView *emotionCard;

@property (nonatomic, assign) id<CardHearColorDelegate> headerDeledate;
@property (nonatomic, assign) id<CardsDataControllerDelegate> dataDelegate;

-(void)clearCardView:(NSInteger)idx;
-(void)updateLocationCard:(NSString*)location;

@end
