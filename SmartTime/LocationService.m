//
//  LocationService.m
//  SmartTime
//
//  Created by tsia on 15-6-15.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "LocationService.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

#define APIKEY      @"515cc206739a8c74a748fc06500b63fb"

@interface LocationService() <MAMapViewDelegate, AMapSearchDelegate>

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, strong) CLLocation *currentLocation;

@end

@implementation LocationService

+(LocationService *)sharedInstance{
    
    static LocationService *sharedManager;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[LocationService alloc] init];
    });
    
    return sharedManager;
}

// 开始定位一次，定位成功后会异步调用代理的方法
-(void)startLocate{
    
    [self initMapView];
    [self initSearch];
}

-(void)initMapView{
    
    [MAMapServices sharedServices].apiKey = APIKEY;
    _mapView = [[MAMapView alloc] init];
    _mapView.delegate = self;
    
    _mapView.showsUserLocation = YES;   // 开启定位
}

- (void)initSearch{
    
    _search = [[AMapSearchAPI alloc] initWithSearchKey:APIKEY Delegate:self];
}

#pragma mark - MAMapViewDelegate
// 定位成功回调
- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
    
    NSLog(@"地理位置获取成功: %@", userLocation.location);
    
    _currentLocation = [userLocation.location copy];
    
    // 成功获取到了位置，开始反地理编码
    if (_currentLocation){
        
        AMapReGeocodeSearchRequest *request = [[AMapReGeocodeSearchRequest alloc] init];
        request.location = [AMapGeoPoint locationWithLatitude:_currentLocation.coordinate.latitude longitude:_currentLocation.coordinate.longitude];
        [_search AMapReGoecodeSearch:request];
        
        _mapView.showsUserLocation = NO;    // 关闭定位，只要一次定位
    }
}
// 定位失败回调
- (void)mapView:(MAMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    NSLog(@"地理位置获取失败, error :%@", error);
    _mapView.showsUserLocation = NO;   // 关闭定位
    [_delegate updateLocationForUI:nil IsSuccess:NO];
}

#pragma mark - AMapSearchDelegate

- (void)searchRequest:(id)request didFailWithError:(NSError *)error{
    NSLog(@"反地理编码请求失败 :%@, error :%@", request, error);
    _mapView.showsUserLocation = NO;   // 关闭定位
    [_delegate updateLocationForUI:nil IsSuccess:NO];
}

- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    //NSLog(@"response :%@", response);
    
    NSString *detailaddress = response.regeocode.formattedAddress;  // 详细地址
    [_delegate updateLocationForUI:detailaddress IsSuccess:YES];
    
}

@end
