//
//  STimeTableData.m
//  SmartTime
//
//  Created by tsia on 15-5-30.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "STimeTableData.h"
#import "STimeTableDao.h"

@implementation STimeTableData

/**
 *  时光轴列表的数据，单例对象
 *
 *  @return 单例对象
 */
+(STimeTableData *)sharedInstance{
    
    static STimeTableData *sharedSTimeData;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSTimeData = [[STimeTableData alloc] init];
    });
    
    return sharedSTimeData;
}

-(instancetype)init{
    
    self = [super init];
    if (self) {
        
        [self freshData];
    }
    
    return self;
}

-(void)freshData{
    
    STimeTableDao *dao = [[STimeTableDao alloc]init];
    
//    for (int i = 0; i<100; i++) {
//        SaveDiaryInfo *info = [[SaveDiaryInfo alloc]init];
//        info.datetime = @"2015-04-12 12:12:12";
//        info.text = @"啊啊啊啊啊啊啊啊啊啊啊啊啊啊啊哈哈哈哈哈哈吧啊啊";
//        info.imgnum = @"sds";
//        [dao insertItem:info];
//        
//        info = [[SaveDiaryInfo alloc]init];
//        info.datetime = @"2014-01-01 00:00:00";
//        info.location = @"靖江市新桥镇";
//        info.imgnum = @"3";
//        [dao insertItem:info];
//        
//        info = [[SaveDiaryInfo alloc]init];
//        info.datetime = @"2011-11-11 11:11:11";
//        info.text = @"打发士大夫似的公司发的规范施工";
//        info.imgnum = @"4";
//        [dao insertItem:info];
//        
//        info = [[SaveDiaryInfo alloc]init];
//        info.datetime = @"2011-11-11 11:11:11";
//        info.text = @"打发士大夫似的公司发的规范施工";
//        info.imgnum = @"1";
//        [dao insertItem:info];
//        
//        info = [[SaveDiaryInfo alloc]init];
//        info.datetime = @"2011-11-11 11:11:11";
//        info.text = @"打发士大夫似的公司发的规范施工";
//        info.imgnum = @"2";
//        [dao insertItem:info];
//
//    }
    

    _listData = dao.queryAllItems;
    
    _sumDay = dao.daySum;
    _sumDiary = [NSString stringWithFormat:@"%lu", _listData.count];
    _sumImage = dao.imageSum;

}

@end
