//
//  TimeLineView.m
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "TimeLineView.h"
#import "UIColor+Util.h"

#define TIMELINE_WIDTH      2
#define GREENDOT_WIDTH      15
#define GREENDOT_HEIGHT     15

@implementation TimeLineView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    
    return self;
}

/**
 *  时光轴线，固定宽度，高度充满父view
 */
-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake((self.frame.size.width - TIMELINE_WIDTH)/2, 0, TIMELINE_WIDTH, self.frame.size.height)];
    lineView.backgroundColor = [UIColor colorFromRGB:0xd5d5d5];
    [self addSubview:lineView];
    
    
    UIImageView *greendot = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width - GREENDOT_WIDTH)/2, 23.f, GREENDOT_WIDTH, GREENDOT_HEIGHT)];
    greendot.image = [UIImage imageNamed:@"green_dot.png"];
    [self addSubview:greendot];
    
}

@end
