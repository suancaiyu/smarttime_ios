//
//  SaveDiaryInfo.h
//  SmartTime
//
//  Created by tsia on 15-6-3.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@interface SaveDiaryInfo : NSObject
/**
 *
 * id INTEGER (primary key autoincrement)
 * datetime     TEXT
 * week     TEXT
 * text     TEXT
 * location TEXT
 * weather  TEXT
 * emotion TEXT  表情
 * audio   TEXT  录音
 * imgnum  TEXT  照片数量
 * image1  TEXT  照片1的路径
 * image2  TEXT  照片2的路径
 * image3  TEXT  照片3的路径
 * image4  TEXT  照片4的路径
 *
 */

@property (nonatomic, strong)NSString *datetime;
@property (nonatomic, strong)NSString *week;
@property (nonatomic, strong)NSString *text;
@property (nonatomic, strong)NSString *location;
@property (nonatomic, strong)NSString *weather;
@property (nonatomic, strong)NSString *emotion;
@property (nonatomic, strong)NSString *audio;
@property (nonatomic, strong)NSString *imgnum;
@property (nonatomic, strong)NSString *image1;
@property (nonatomic, strong)NSString *image2;
@property (nonatomic, strong)NSString *image3;
@property (nonatomic, strong)NSString *image4;

@end
