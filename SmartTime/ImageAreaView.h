//
//  ImageAreaView.h
//  SmartTime
//
//  Created by tsia on 15-6-2.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@interface ImageAreaView : UIView

@property (nonatomic, assign)CGFloat height;

-(instancetype)initWithWidth:(CGFloat)givenwidth Index:(NSInteger)index;

@end
