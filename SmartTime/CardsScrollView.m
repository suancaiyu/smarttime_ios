//
//  CardsScrollView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CardsScrollView.h"

@interface CardsScrollView()

@end

@implementation CardsScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI{
    
    // 不可以拖拽滑动，只可以通过点击卡片栏
    self.scrollEnabled = NO;
    
    // 滚动范围
    self.contentSize = Size(ViewWidth(self)*4, 220);
    // 隐藏滚动条
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
    
    // 添加子card
    _locationCard = [[LocationCardView alloc]initWithFrame:Rect(0, 0, ViewWidth(self), 220)];
    [self addSubview:_locationCard];
    
    _photoCard = [[PhotoCardView alloc]initWithFrame:Rect(ViewWidth(self), 0, ViewWidth(self), 220)];
    [self addSubview:_photoCard];
    
    _weatherCard = [[WeatherCardView alloc]initWithFrame:Rect(ViewWidth(self)*2, 0, ViewWidth(self), 220)];
    [self addSubview:_weatherCard];
    
    _emotionCard = [[EmotionCardView alloc]initWithFrame:Rect(ViewWidth(self)*3, 0, ViewWidth(self), 220)];
    [self addSubview:_emotionCard];
    
    // 初始化index
    self.curIndex = 0;
    
}

// 只通过这个函数切换卡片
-(void)setCurIndex:(NSInteger)curIndex{
    
    _curIndex = curIndex;
    [self setContentOffset:CGPointMake(ViewWidth(self)*curIndex, 0) animated:YES]; // 动画滚动视图
}


-(void)setHeaderDeledate:(id<CardHearColorDelegate>)headerDeledate{
    
    _locationCard.headerDelegte = headerDeledate;
    _weatherCard.headerDelegte = headerDeledate;
    _photoCard.headerDelegte = headerDeledate;
    _emotionCard.headerDelegte = headerDeledate;
}

-(void)clearCardView:(NSInteger)idx{
    
    switch (idx) {
        case 0:
            [_locationCard clearLocation];
            break;
        case 1:
            // do nothing
            break;
        case 2:
            [_weatherCard clearSelected];
            break;
        case 3:
            [_emotionCard clearSelected];
            break;
            
        default:
            break;
    }
}

-(void)updateLocationCard:(NSString*)location{
    [_locationCard updateLocation:location];
}

-(void)setDataDelegate:(id<CardsDataControllerDelegate>)dataDelegate{
    _locationCard.dataDelegate = dataDelegate;
    _weatherCard.dataDelegate = dataDelegate;
    _emotionCard.dataDelegate = dataDelegate;
    _photoCard.pickerDelegate = dataDelegate;
}

@end
