//
//  CustomNaviBarView.h
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface CustomNaviBarView : UIView

@property (nonatomic, weak)UIViewController *parentViewController;

+(UIImageView*)createNaviBarImageView:(NSString*)img target:(id)target action:(SEL)action;
-(void)setLeftFirstImageView:(UIImageView*)iv;
-(void)setLeftSecondImageView:(UIImageView*)iv;
- (void)setTitle:(NSString *)title;
-(void)setRightFirstImage:(UIImageView*)iv;
-(void)setRightSecondImage:(UIImageView*)iv;

@end
