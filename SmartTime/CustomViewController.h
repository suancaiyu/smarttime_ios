//
//  CustomViewController.h
//  SmartTime
//
//  Created by tsia on 15-6-5.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CustomNaviBarView.h"

@interface CustomViewController : UIViewController


- (void)setNaviBarLeftFirstIv:(UIImageView *)iv;
- (void)setNaviBarLeftSecondIv:(UIImageView *)iv;
-(void)setNaviBarTitle:(NSString*)title;
-(void)setNaviBarRightFirstIv:(UIImageView *)iv;
-(void)setNaviBarRightSecondIv:(UIImageView *)iv;

@end
