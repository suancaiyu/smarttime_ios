//
//  LocationCardView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "LocationCardView.h"
#import "UILabel+Util.h"
#import "UIColor+Util.h"

#define HINTTEXT_COLOR      0x808080
#define NORMALTEXT_COLOR    0x000000

@interface LocationCardView() <UIAlertViewDelegate>

@property (nonatomic, strong) UILabel *lbtext;  // 地点文字

@end

@implementation LocationCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    _lbtext = [[UILabel alloc]init];
    _lbtext.numberOfLines = 0;   // 自动换行
    _lbtext.textAlignment = NSTextAlignmentCenter;
    
    // 单击事件
    _lbtext.userInteractionEnabled = YES;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
    [_lbtext addGestureRecognizer:recognizer];
    
    [self clearLocation];
    
    [self addSubview:_lbtext];
    
}

-(void)handlerTapGesture:(UITapGestureRecognizer*)recognizer{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"自定义位置" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];
    
    // 初始化位置文字
    if (![_lbtext.text isEqualToString:@"暂无位置信息"]) {
        [alertView textFieldAtIndex:0].text = _lbtext.text;
    }
}

#pragma mark - UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) { // 点击确定
        
        // 更新UI
        [self updateLocation:[alertView textFieldAtIndex:0].text];
        [_headerDelegte setCardHeadColor:0 Green:YES];
        // 更新数据
        [_dataDelegate setlocation:[alertView textFieldAtIndex:0].text];
    }
}

// 清除位置信息
-(void)clearLocation{
    // 清除UI显示
    _lbtext.font = [UIFont fontWithName:@"HelveticaNeue" size:20.0f];
    _lbtext.text = @"暂无位置信息";
    _lbtext.textColor = [UIColor colorFromRGB:HINTTEXT_COLOR];
    CGSize textSize = [_lbtext textAreaSizeWithWidth:ViewWidth(self)-18-18];
    _lbtext.frame = Rect(18, (ViewHeight(self)-textSize.height)/2 - 6, ViewWidth(self)-18-18, textSize.height);
    
    // 清除数据
    [_dataDelegate setlocation:nil];
}

// 更新获取的位置信息
-(void)updateLocation:(NSString*)location{
    
    _lbtext.text = location;
    _lbtext.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    _lbtext.textColor = [UIColor colorFromRGB:NORMALTEXT_COLOR];
    CGSize textSize = [_lbtext textAreaSizeWithWidth:ViewWidth(self)-18-18];
    _lbtext.frame = Rect(18, (ViewHeight(self)-textSize.height)/2 - 6, ViewWidth(self)-18-18, textSize.height);
}

@end
