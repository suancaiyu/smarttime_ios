//
//  PhotoCardView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CardsViewDelegate.h"

@interface PhotoCardView : UIView

@property (nonatomic, strong) NSArray *photoIvs;
@property (nonatomic, assign) id<CardHearColorDelegate> headerDelegte;
@property (nonatomic, assign) id<CardsDataControllerDelegate> pickerDelegate;

-(void)readLocalImage:(NSURL*)path;
-(void)showImageWithPathArry:(NSArray*)arrimg;

@end
