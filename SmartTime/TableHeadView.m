//
//  TableHeadView.m
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "TableHeadView.h"

#define HEAD_WIDTH      90
#define HEAD_HEIGHT     90
#define HEAD_MARGIN_LEFT      15

#define SUM_MARGIN_LEFT       5
#define SUM_MARGIN_BOTTOM     13

@implementation TableHeadView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI{
    
    // 首页
    _firstpage = [[UIImageView alloc]initWithFrame:Rect(0, 0, self.frame.size.width, 200)];
    _firstpage.image = [UIImage imageNamed:@"firstpage.jpg"];
    _firstpage.contentMode = UIViewContentModeScaleAspectFill;
    _firstpage.clipsToBounds = YES;
    [self addSubview:_firstpage];
    
    // 头像
    _head = [[UIImageView alloc]initWithFrame:Rect(HEAD_MARGIN_LEFT, 245-HEAD_HEIGHT, HEAD_WIDTH, HEAD_HEIGHT)];
    _head.image = [UIImage imageNamed:@"head.jpg"];
    _head.contentMode=UIViewContentModeScaleAspectFill;
    _head.clipsToBounds = YES;
    CALayer *layer = [_head layer];
    [layer setMasksToBounds:YES];
    [layer setBorderWidth:3];
    [layer setBorderColor:[UIColor whiteColor].CGColor];
    [self addSubview:_head];
    
    // 统计条
    CGFloat sumviewWidth = ScreenWidth - HEAD_MARGIN_LEFT - HEAD_WIDTH - SUM_MARGIN_LEFT;//给统计栏的最大宽度
    _diarySummaryView = [[DiarySummaryView alloc]initWithWidth:sumviewWidth];
    _diarySummaryView.frame = Rect(CGRectGetMaxX(_head.frame)+SUM_MARGIN_LEFT, CGRectGetMaxY(_head.frame)-SUM_MARGIN_BOTTOM-_diarySummaryView.framesize.height, _diarySummaryView.framesize.width, _diarySummaryView.framesize.height);
    [self addSubview:_diarySummaryView];
    
    
}

@end
