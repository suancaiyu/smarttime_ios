//
//  WeatherCardView.m
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "WeatherCardView.h"
#import "LBorderView.h"

#define ROWS        2
#define COLUMNS     4

#define IMAGE_SIZE    64        // ImageView宽高固定64pt

#define MARGIN_LEFT     4
#define VIEW_HEIGHT     80      // 边框的高度

@interface WeatherCardView(){
    
    CGFloat _viewwidth;
    CGFloat _vergap;
}

@property (nonatomic, strong) NSArray *arrWea;          // 天气文字字符串数组
@property (nonatomic, strong) NSArray *arrImages;
@property (nonatomic, strong) LBorderView *borderView;      // 用来显示边框的view
@property (nonatomic, strong) NSArray *arrIvs;

@end

@implementation WeatherCardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initUI];
    }
    return self;
}

-(void)initData{
    
    _arrWea = @[@"晴", @"多云", @"阴", @"小雨", @"中雨", @"暴雨", @"雷雨", @"小雪"];
    
    _viewwidth = (ViewWidth(self)-MARGIN_LEFT*2)/COLUMNS;        // 边框宽度
    _vergap = (ViewHeight(self)-VIEW_HEIGHT*ROWS)/(ROWS+1);   // 纵向的间隔
    
    _arrImages = @[@"wea_qin.png", @"wea_duoyun.png", @"wea_yin.png", @"wea_xiaoyu.png", @"wea_zhongyu.png", @"wea_baoyu.png", @"wea_leiyu.png", @"wea_xiaoxue.png"];
    
    int count = ROWS*COLUMNS;
    NSMutableArray *arriv = [[NSMutableArray alloc]initWithCapacity:count];
    for (int i=0; i<count; i++) {
        
        UIImageView *iv = [[UIImageView alloc]initWithImage:[UIImage imageNamed:_arrImages[i]]];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.tag = i; // 0-7
        
        // 添加单击手势识别
        UITapGestureRecognizer *recognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handlerTapGesture:)];
        iv.userInteractionEnabled = YES;
        [iv addGestureRecognizer:recognizer];
        
        [arriv addObject:iv];
    }
    
    _arrIvs = [[NSArray alloc]initWithArray:arriv];
}

-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    // 边框view
    _borderView = [[LBorderView alloc]initWithFrame:CGRectZero];
    _borderView.borderType = BorderTypeDashed;
    _borderView.dashPattern = 6;
    _borderView.spacePattern = 2;
    _borderView.borderWidth = 0.5;
    _borderView.cornerRadius = 1;
    [self addSubview:_borderView];
    
    // 布局天气图片
    for (int i=0; i<ROWS; i++) {
        for (int j=0; j<COLUMNS; j++) {
            NSInteger idx = COLUMNS*i + j;
            
            UIImageView *iv = _arrIvs[idx];
            iv.frame = Rect(MARGIN_LEFT+_viewwidth*j+ (_viewwidth-IMAGE_SIZE)/2, _vergap+(VIEW_HEIGHT+_vergap)*i+ (VIEW_HEIGHT-IMAGE_SIZE)/2, IMAGE_SIZE, IMAGE_SIZE);
            [self addSubview:iv];
        }
    }
    
}

/**
 *  设置布局图片的边框
 *
 *  @param index 表示第几张，从0开始
 */
-(void)setBordeViewFrameWithIndex:(NSInteger)index{
    
    NSInteger i = index/COLUMNS;
    NSInteger j = index%COLUMNS;
    
    _borderView.frame = Rect(MARGIN_LEFT+_viewwidth*j, _vergap+(_vergap+VIEW_HEIGHT)*i, _viewwidth, VIEW_HEIGHT);
}

// 单击事件响应，根据tag判断哪一个view
-(void)handlerTapGesture:(UITapGestureRecognizer*)recognizer{
    
    NSInteger tag = recognizer.view.tag;
    // 添加边框
    [self setBordeViewFrameWithIndex:tag];
    
    // 天气栏图标绿显
    [_headerDelegte setCardHeadColor:2 Green:YES];
    
    // 保存天气文字
    [_dataDelegate setWeather:_arrWea[tag]];

}

-(void)clearSelected{
    
    _borderView.frame = CGRectZero;
    // 清空保存的数据
    [_dataDelegate setWeather:nil];
}


@end
