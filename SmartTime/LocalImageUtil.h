//
//  LocalImageUtil.h
//  SmartTime
//
//  Created by tsia on 15-6-11.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "AssetsLibrary/AssetsLibrary.h"

@interface LocalImageUtil : NSObject

+(void)readImageFromLocalPath:(NSURL*)path ToImageView:(UIImageView*)iv;
+(void)checkSupportSource;

+(BOOL)isSupportSourceCamera;
+(BOOL)isSupportSourcePhotoLibrary;
+(BOOL)isSupportSourcePhotosAlbum;

@end
