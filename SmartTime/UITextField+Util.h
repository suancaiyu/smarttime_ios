//
//  UITextField+Util.h
//  SmartTime
//
//  Created by tsia on 15-6-12.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

@interface UITextView(Util)

-(CGSize)textAreaSizeWithWidth:(CGFloat)width;

@end
