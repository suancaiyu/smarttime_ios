//
//  CardHeadView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

/**
 *  CardHeader单击需要的代理，由BottomView来实现
 */
@protocol CardHeaderProtocol <NSObject>

// 滚动到指定的card
-(void)scrollToCard:(NSInteger)index;

// 对应的卡片内容显示清空，位置、天气、表情
-(void)clearCardView:(NSInteger)index;

@end

/**
 *  CardHeader需要控制器来代理的方法，由ReadEditViewController实现
 */
@protocol CardHeaderControllerProtocol <NSObject>

-(void)requireLocation;
-(void)hideKeyboard;
-(BOOL)isKeyboardShown;

@end


@interface CardHeadView : UIView

@property (nonatomic, strong) UIImageView *ivlocation;
@property (nonatomic, strong) UIImageView *ivphoto;
@property (nonatomic, strong) UIImageView *ivweather;
@property (nonatomic, strong) UIImageView *ivemotion;

@property (nonatomic, assign) id<CardHeaderProtocol> delegate;
@property (nonatomic, assign) id<CardHeaderControllerProtocol> controllerDelegate;

-(void)setHeaderIndex:(NSInteger)index Green:(BOOL)isGreen;

@end
