//
//  UIColor+Util.h
//  SmartTime
//
//  Created by tsia on 15-6-1.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface UIColor(Util)

+ (UIColor *)colorFromRGB: (NSInteger)rgbValue;

@end
