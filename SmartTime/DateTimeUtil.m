//
//  DateTimeUtil.m
//  SmartTime
//
//  Created by tsia on 15-6-3.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "DateTimeUtil.h"

@implementation DateTimeUtil

/**
 *  得到当前的时间
 *
 *  @return 格式：yyyy-MM-dd HH:mm:ss
 */
+(NSString*)nowTime{
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [formatter stringFromDate:date];
}

/**
 *  以下函数都是根据datetime来计算（yyyy-MM-dd HH:mm:ss）
 */

// 得到年yyyy
+(NSString*)yearFromDateTime:(NSString*)datetime{
    
    return [datetime substringWithRange:NSMakeRange(0, 4)];
}

// 得到月MM
+(NSString*)monthFromDateTime:(NSString*)datetime{
    
    return [datetime substringWithRange:NSMakeRange(5, 2)];
}

// 得到日dd
+(NSString*)dayFromDateTime:(NSString*)datetime{
    
    return [datetime substringWithRange:NSMakeRange(8, 2)];
}
/*
Mon
Tue
Wed
Thu
Fri
Sat
Sun
 */
// 得到星期，默认Mon、Tue.. ，转换成"周一"...
+(NSString*)weekFromDateTime:(NSString*)datetime{
    
    NSDictionary *dict = @{@"Mon":@"周一", @"Tue":@"周二", @"Wed":@"周三", @"Thu":@"周四", @"Fir":@"周五", @"Sat":@"周六", @"Sun":@"周日"};
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [formatter dateFromString:datetime];
    NSDateFormatter *weekformatter = [[NSDateFormatter alloc]init];
    [weekformatter setDateFormat:@"E"];
    
    return [dict objectForKey:[weekformatter stringFromDate:date]];
}

// 得到日期yyyy-MM-dd
+(NSString*)dateFromDateTime:(NSString*)datetime{
    
    return [datetime substringWithRange:NSMakeRange(0, 10)];
}

// 得到时间HH:mm:ss
+(NSString*)timeFromDateTime:(NSString*)datetime{
    
    return [datetime substringWithRange:NSMakeRange(11, 8)];
}

@end
