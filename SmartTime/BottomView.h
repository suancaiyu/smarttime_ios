//
//  BottomView.h
//  SmartTime
//
//  Created by tsia on 15-6-7.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import "CardHeadView.h"
#import "CardsScrollView.h"
#import "CardsViewDelegate.h"

@interface BottomView : UIView <CardHeaderProtocol, CardHearColorDelegate>

@property (nonatomic, strong) CardHeadView *cardHead;
@property (nonatomic, strong) CardsScrollView *cardsView;
@property (nonatomic, assign) id<CardHeaderControllerProtocol> controllerDelegate;
@property (nonatomic, assign) id<CardsDataControllerDelegate> dataDelegate;

-(void)updateLocationCard:(NSString*)location;

@end
