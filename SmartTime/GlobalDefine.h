//
//  GlobalDefine.h
//  SmartTime
//
//  Created by tsia on 15-6-6.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#ifndef SmartTime_GlobalDefine_h
#define SmartTime_GlobalDefine_h


#ifdef DEBUG
#define DebugLog(...) NSLog(__VA_ARGS__)
#define DebugMethod() NSLog(@"%s", __func__)
#else
#define DebugLog(...)
#define DebugMethod()
#endif


/////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Redefine


#define MainScreen                          [UIScreen mainScreen]
#define ScreenRect                          [[UIScreen mainScreen] bounds]
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height

#define ViewWidth(v)                        v.frame.size.width
#define ViewHeight(v)                       v.frame.size.height
#define ViewX(v)                            v.frame.origin.x
#define ViewY(v)                            v.frame.origin.y
#define SelfViewWidth                       self.view.bounds.size.width
#define SelfViewHeight                      self.view.bounds.size.height
#define RectX(f)                            f.origin.x
#define RectY(f)                            f.origin.y
#define RectWidth(f)                        f.size.width
#define RectHeight(f)                       f.size.height
#define RectSetWidth(f, w)                  CGRectMake(RectX(f), RectY(f), w, RectHeight(f))
#define RectSetHeight(f, h)                 CGRectMake(RectX(f), RectY(f), RectWidth(f), h)
#define RectSetX(f, x)                      CGRectMake(x, RectY(f), RectWidth(f), RectHeight(f))
#define RectSetY(f, y)                      CGRectMake(RectX(f), y, RectWidth(f), RectHeight(f))
#define RectSetSize(f, w, h)                CGRectMake(RectX(f), RectY(f), w, h)
#define RectSetOrigin(f, x, y)              CGRectMake(x, y, RectWidth(f), RectHeight(f))
#define Rect(x, y, w, h)                    CGRectMake(x, y, w, h)
#define Size(w, h)                          CGSizeMake(w, h)

#define StatusBarHeight                     [UIApplication sharedApplication].statusBarFrame.size.height
#define SelfDefaultToolbarHeight            self.navigationController.navigationBar.frame.size.height
#define IOSVersion                          [[[UIDevice currentDevice] systemVersion] floatValue]
#define IsiOS7Later                         !(IOSVersion < 7.0)



//////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - app define

#define STimeNaviBarHeight                  48

// 如果存入的是nil，那么读出来的字符串就是@"(null)"。根据字符串，判断字段是否为“空”。   （用于text、location、weather、emotion显示判断）
#define IS_DBTEXT_NOTEMP(TEXT)     ((TEXT).length>0 && ![(TEXT) isEqualToString:@"(null)"])

// intValue==0那么str可能是@"0" @"sdf" nil @"(null)"等等, >0只有一种情况，即为整数字符串。（用于imgnum显示判断）
#define IS_DBIMGNUM_NOTZERO(TEXT)      ([(TEXT) intValue]>0)


#endif
