//
//  DateTimeUtil.h
//  SmartTime
//
//  Created by tsia on 15-6-3.
//  Copyright (c) 2015年 tsia. All rights reserved.
//


@interface DateTimeUtil : NSObject

+(NSString*)nowTime;
+(NSString*)yearFromDateTime:(NSString*)datetime;
+(NSString*)monthFromDateTime:(NSString*)datetime;
+(NSString*)dayFromDateTime:(NSString*)datetime;
+(NSString*)dateFromDateTime:(NSString*)datetime;
+(NSString*)timeFromDateTime:(NSString*)datetime;
+(NSString*)weekFromDateTime:(NSString*)datetime;

@end
