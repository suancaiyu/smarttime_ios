//
//  main.m
//  SmartTime
//
//  Created by tsia on 15/5/29.
//  Copyright (c) 2015年 tsia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
